


<div class="responsive-table"> 

	<h4 class="header"> Liste des équipes</h4>

	<div class="row">


		<p>Ici, vous voyez <code class="  language-markup">la liste complète </code> des équipes.</p>


		<div class="col s12 m12 l12">
			<form action='' method='post' >
				<table class="responsive-table centered">
					<thead>
						<tr>
							<th>Numéro de l'équipe</th>
							<th>Nom de l'équipe</th>
							<th>Nom du capitaine</th>
							<th>Sport</th>
							<th>Nombre de parties jouées</th>
							<th>Nb joueurs/Nb max joueurs</th>
							<th>Date de création</th>
							<th>Options</th>

						</tr>
					</thead>
					<tbody>
						
						<?php
						$i =1;


						foreach($data as $contenu)
						{
							include('tableauequipes.php');
							$i++;
						}
						?>

					</tbody>
				</table>
			</form>
		</div>






	</div>
</div>
