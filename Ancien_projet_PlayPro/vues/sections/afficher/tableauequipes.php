	<?php
	if ($contenu!=null)
	{
		?>
		<tr>

			<td>  <?php echo $i ?></td>
			<td><a href='?action=profil' ><?php echo $contenu->getNom_equipe(); ?></a></td>
		<?php 
			if (!is_null($contenu->getCapitaine())){
				?>
				<td><?php echo $contenu->getCapitaine()->getNom(); ?> </td>

				<?php
			}else{
				?>
				<td>Pas de capitaine</td>
				<?php
			}
 		?>
			
			
			<td><?php echo $contenu->getSport(); ?> </td>
			<td><a href='?action=afficherparties&id=<?php echo $contenu->getNom_equipe() ?>' title = 'Voir toutes les parties de <?php echo $contenu->getNom_equipe(); ?> '><?php echo $contenu->getNb_parties_jouees() ?> 
		</a>
	</td>
	<td>
		<a href='?action=afficherjoueurs&id=<?php echo $contenu->getNom_equipe() ?>' title = 'Voir la liste des joueurs de <?php echo $contenu->getNom_equipe(); ?> '><?php echo $contenu->getNb_joueurs()."/".$contenu->getNb_max_joueurs();?>
		
	</a>
</td>
<td><?php echo $contenu->getDate_creation() ?></td>

<?php  
if ((!is_null($contenu->getCapitaine()))&&($contenu->getCapitaine()->getPseudo() == $_SESSION['connected'] ))
{
	?>
	<td>
		<div  class='col s12  left' >
			<form method="post" action="./index.php">
				<input name='nom' type='hidden' value = '<?php echo $contenu->getNom_equipe()?>' />
				<button type='submit' title="Supprimer" name='action' value='supprimerequipe'   class='col s6 btn waves-effect blue left'><i class='far fa-trash-alt'></i>
				</button>
				
				
				<button type='submit'   title='Modifier' name='action' value='modifierequipe' class='col s6 btn waves-effect blue right modal-trigger '>
					<i class='fas fa-eraser'></i>
				</button>
			</form>
			
		</div>
	</td>
	<?php
}else
{  require_once('./modele/MembreDAO.class.php');
$udao = new MembreDAO();
$joueur = $udao->findBytitre($_SESSION["connected"]);
?>
<td>
	<div class='col s12  left'>
		<form method="post" action="./index.php">

			<input name='nom' type='hidden' value = '<?php echo $contenu->getNom_equipe()?>'/>

			<?php if ($joueur->getEquipe() === $contenu->getNom_equipe()) { ?>

				<button type='submit' name='action' value='quitterequipe'	title='Quitter cette équipe'  
				class='col s12 btn waves-effect red center'>
				<i class='fas fa-arrow-circle-right'>	
				</i>
			</button>

		<?php }else{ ?>

			<button type='submit' name='action' value='integrerequipe'	title='Integrer cette équipe'  
			class='col s12 btn waves-effect green center'>
			<i class='fas fa-arrow-circle-left'>	
			</i>
		</button>
	<?php } ?>
</form>
</div>
</td>
<?php
} 
?>
</tr>
<?php
} 
?>
