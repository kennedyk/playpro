<?php
if(!is_null(self::getMessage())) {
	echo '<div class="green  lighten-4 m3 center alert alert-success alert-dismissible fade show rang" role="alert">';

	echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
	echo '<span aria-hidden="true">&times;</span>';
	echo '</button>';
	echo self::getMessage();
	echo '</div>';
}
?>

