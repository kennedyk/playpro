<?php include('./vues/sections/afficher/affichermessage.php'); ?>

<div class="responsive-table"> 
	<h4 class="header"> Liste des parties</h4>
	<div class="row">
		<p>Ici, vous voyez <code class="  language-markup">la liste complète </code> des équipes.</p>

		<div class="col s12 m12 l12">
			<table class="responsive-table centered">
				<thead>
					<tr>
						<th>Type de sport</th>
						<th>Lieu</th>
						<th>Équipe 1</th>
						<th>Équipe 2</th>
						<th>Date - Heure</th>
						<th>Score</th>
						<th>Options</th>

					</tr>
				</thead>
				<tbody>
					
					<?php
					
					
					$edao = new EquipeDAO();


					$udao = new MembreDAO();



					foreach($data as $contenu)

					{

						if ($contenu!=null)
						{

							echo "<tr>";


							echo	"<td>".$contenu->getSport()."</td>";
							echo	"<td>".$contenu->getlieu_partie()."</td>";
							echo	"<td>".$contenu->getEquipe_1()."</td>";
							echo	"<td>".$contenu->getEquipe_2()."</td>";
							echo	"<td>".$contenu->getDate_partie_heure()."</td>";
							echo	"<td>".$contenu->getScore()."</td>";

							?>

							<?php  
							$equipe=$edao->findBytitre($contenu->getEquipe_1());
							$user= $udao->findById($equipe->getCapitaine());

							if (isset($_SESSION['connected'])){

								if ((!is_null($user)) && ($user->getPseudo() == $_SESSION['connected'] )) {


									?>
									<td>
										<div  class='col s12  left' >
											<form method="post" action="./index.php">
												<input name='id_partie' type='hidden' value = '<?php echo $contenu->getId_Partie() ?>'/>
												<button type='submit' title="Supprimer" name='action' value='supprimerpartie'   class='col s6 btn waves-effect blue left'><i class='far fa-trash-alt'></i>
												</button>

												<input name='id_partie' type='hidden' value = '<?php echo $contenu->getId_Partie() ?>'/>
												<button type='submit'   title='Modifier' name='action' value='modifierpartie' class='col s6 btn waves-effect blue right modal-trigger '>
													<i class='fas fa-eraser'></i>
												</button>
											</form>


										</div>
									</td>
									<?php  

								}


							}

							else{ ?>
								<td>
									<div  class='col s12  left' >
										<a href="#modal2" title="Rejoindre la partie" class='col s12 btn waves-effect blue center modal-trigger '>
											<i class="material-icons">account_circle</i>
										</a>
									</div>
								</td>

								<?php
							}	

							echo "</tr>";
						}

					}

					?>


				</tbody>
			</table>
		</div>

	</div>

</div>


