</br></br>
<div class="row" style="position: center; margin-left: 50px; margin-right: 50px;">
    <div class="col s12 m12">
      <div class="card cyan darken-3">
        <div class="card-content black-text center">

          <span class="card-title ">Quoi ?</span>
          <p>Un simple mot peut stimuler une immense interrogation! </br>
          Peu importe l'ambiguité ou la curiosité que ceci peut provoquer la réponse
      est très simple: </br>
  ce site pour permettra de vous épanouir et défier les contraintes du temps afin d'exercer votre passion.</br>
	Vous déesirez plannifier une partie ou en joindre une. C'est la meilleur place.</p>

  </br>
  <span class="card-title ">Les avantages de PlayPro :</span>
  <p> D'abord, c'est service gratuit. Ensuite, vous pouvez organiser des parties, créer des équipes où y adhérer.</p>

  </br>
  <span class="card-title ">Les différents types de membres :</span>
  <p>- Visiteur :</br>
  	1.	Se renseigner sur le fonctionnement général de la plateforme</br>
2.	Consulter les parties existantes </br>
3.	Effectuer des recherches multicritères sur les parties</br>

  </p>
  <p>- Joueur : </br>
  
5.	Gérer une équipe </br>
6.	Gérer une partie</br>
7.	Intégrer des équipes existantes</br></p>
</br>

</br>
  <span class="card-title ">Désirez-vous faire un test ?</span>
  <p> Nous vous invitons de visiter l'onglet Comment? pour commencer. Vous y trouverez les détails des étapes à suivre pour commencer. Prière de cliquer sur le bouton ci-dessous.</p>


        </div>
        <div class="card-action center">
          <a href="?action=comment">Lire comment?</a>
        </div>
      </div>
    </div>
  </div>