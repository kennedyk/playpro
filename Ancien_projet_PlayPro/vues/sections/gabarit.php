<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">

	<link rel="icon" type="logo/ico" href="./vues/sections/images/logo.ico" />
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

	<!-- style personnalisé -->
	<link rel="stylesheet" type="text/css" href="css/playprostyle.css">
	<link href= "https://github.com/chingyawhao/materialize-clockpicker"/>

	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl"
	crossorigin="anonymous"></script>

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title><?php echo $titre; ?></title>
</head>


<body>
	<div>
		<?php 

		if (!ISSET($_SESSION)) {
			session_start();
		}
		if (ISSET($_SESSION["connected"])){
			
			include("headers/headerjoueur.php");
		}

		else {
			include("headers/header.php");
		}
		

		?>
	</div>
	<main id="content">
		<?php 
		
		echo $contenu; ?>
	</main>
	<div>
		<?php
		include("footer.php");
		?>
	</div>
	<script src="./js/ajax.js"></script>
	<script src="./js/framework.js"></script>
	<script src="./js/gestion_equipes.js"></script>
	<script type="text/javascript">
		const timer = document.querySelector('.timepicker');
		M.timepicker.init(time,{});

	</script>
</body>


	<!--JavaScript at end of body for optimized loading-->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>


	<script>
		$('document').ready(function(){
			$('.sidenav').sidenav();
			$('.slider').slider();
			$('.dropdown-trigger').dropdown();

			$('.tap-target').tapTarget();

			$('select').formSelect();
      // $('.sidenav1').sidenav();
      $('.modal').modal();

  }); 

		$("#aide").click(function(){


			$('.tap-target').tapTarget('open');


		}); 


		document.addEventListener('DOMContentLoaded', function() {
			var elems = document.querySelectorAll('.fixed-action-btn');
			var instances = M.FloatingActionButton.init(elems, {
				direction: 'bottom',
				hoverEnabled: false
			});
		});

		document.addEventListener('DOMContentLoaded', function() {
			var elems = document.querySelectorAll('.datepicker');
			var instances = M.Datepicker.init(elems, {
				format:"yyyy-mm-dd",
				showClearBtn:true
				<!-- i18n{ -->
					<!-- clear:"EFFACER" -->
					<!-- done:"CHOISIR" -->
					<!-- cancel:"ANNULER" -->
					<!-- } -->
				});
		});
		
		
	</script>
	<script src="./js/alert-close.js"></script>

	<script type="text/javascript">
		const timer = document.querySelector('.timepicker');
		M.timepicker.init(time,{
			showClearBtn:true;

		});

	</script>
