

  <!-- Modal Structure -->
  <div id="modal" class="modal">
    <div class="modal-content " >

          <form class="col s12" method="post">
            <div class="row">
              <form class="col s12">
               <div class="row">
                <div class="input-field col s5 left" style="margin-left: 35px;">
                  <input id="pseudonyme" type="text" class="validate">
                  <label for="pseudonyme">Pseudonyme</label>
                </div>
                <div class="input-field col s3">
                  <select>
                    <option value="" disabled selected>Sexe</option>
                    <option value="1">Femme</option>
                    <option value="2">Homme</option>
                  </select>
                </div>
                <div class="input-field col s3">
                  <select multiple>
                    <option value="" disabled selected>Sport préféré</option>
                    <option value="1">Soccer</option>
                    <option value="2">Hockey</option>
                    <option value="3">Badminton</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s5" style="margin-left: 35px;">
                  <input id="prenom" type="text" class="validate">
                  <label for="prenom">Prénom</label>
                </div>
                <div class="input-field col s5 right" style="margin-right: 35px;">
                  <input id="nom" type="text" class="validate">
                  <label for="nom">Nom</label>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s5 left" style="margin-left: 35px;">
                  <input id="password" type="password" class="validate">
                  <label for="password">Mot de passe</label>
                </div>
                  <div class="input-field col s5 right" style="margin-right: 35px;">
                    <input id="password1" type="password" class="validate">
                    <label for="password1">Confirmer Mot de passe</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s5 left" style="margin-left: 35px;">
                    <input id="email" type="email" class="validate">
                    <label for="email">Email</label>
                  </div>
                  <div class="input-field col s5 right" style="margin-right: 35px;">
                    <input id="email1" type="email" class="validate">
                    <label for="email1">Confirmer Email</label>
                  </div>
                </div>

                <div class="row">
                  <div class="col s3 left" style="margin-left: 190px;">
                    <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect waves-light ' >Créer compte</button>
                  </div>
                  <div class="col s3 right" style="margin-right: 190px;">
                    <button type='cancel' name='btn_cancel' class='col s12 btn btn-large waves-effect orange'>Annuler</button>
                  </div>
                </div>
              </div>
            </form>
          </div>

        </form>
      </div>

    </div>

