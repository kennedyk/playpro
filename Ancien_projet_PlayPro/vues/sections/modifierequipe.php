







<div class="container">
  <div class="col s8 m8 offset-m2 l6 offset-l3">
    <div class="card-panel grey lighten-5 z-depth-1">
      <div class="row valign-wrapper">
        
        <div class="col s6">
          <span class="black-text">
            <form method="post" action="./index.php">

            <div class="input-field col s9">
              <input type="hidden" name = "nom" value ="<?php echo $data->getNom_equipe();?>" />
              <input name = "nom" placeholder="<?php echo $data->getNom_equipe();?>"   title ="Vous ne pouvez pas modifier le nom de l'équipe"class="validate" disabled />
              
            </div>

            <div class="input-field col s9">
          
              <select id="sport" name="sport" type="text" class="validate" >
                    <option value="<?php echo $data->getSport()?>" ><?php echo $data->getSport()?> </option>
                    <option value="Soccer">Soccer</option>
                    <option value="Hockey">Hockey</option>
                    <option value="Badminton">Badminton</option>
              </select>
              <label for="sport">Sport</label>
            </div>

            <div class="input-field col s9">
              
              <select name ="nb_max" placeholder="<?php echo $data->getNb_max_joueurs()?>" id="nb"  type="text" class="validate" >
                    <option value="<?php echo $data->getNb_max_joueurs()?>" selected> <?php echo $data->getNb_max_joueurs()?></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>

                  </select>
              <label for="nb">Nombre max de joueurs</label>
            </div>
            <div class ="col s12">
              <div class="col s5 left" style="margin-left: 5px;">
              <button type='submit' name='action' value='enregistrermodifequipe' class='col s12 btn btn-medium waves-effect waves-light ' >Enrégistrer
              </button>
            </div>
                
            <div class="col s5 left" style="margin-left: 5px;">
              <button type='submit' method='post' name='action' value='afficherequipes' class='col s12 btn btn-medium waves-effect waves-light orange ' >ANNULER</button>
            </div>
              
            </div>

          

           </form> 
          </span>
        </div>
      </div>
    </div>
  </div>
</div>