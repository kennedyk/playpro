<?php

  require_once('./modele/classes/Partie.class.php');
  require_once('./modele/classes/Equipe.class.php');
  require_once('./modele/EquipeDAO.class.php');

  $Equipes = array();
  $edao = new EquipeDAO();
  $Equipes = $edao->findAll();
  $EquipeInvite = $edao->findBytitre('Invité');

?>







<div class="container">
  <div class="col s8 m8 offset-m2 l6 offset-l3">
    <div class="card-panel grey lighten-5 z-depth-1">
      <div class="row valign-wrapper">
        
        <div class="col s6">
          <span class="black-text">
            <form method="post" action="./index.php">

            <div class="input-field col s9">
              
              <input name = "typeSport" placeholder="<?php echo $data->getSport();?>"   title ="Vous ne pouvez pas modifier le type du sport"class="validate" disabled />
              
            </div>
          
            <div class="input-field col s9">

              <input name = "lieuPartie" placeholder="<?php echo $data->getLieu_partie();?>"    title ="Modifier lieu de partie" value="<?php echo $data->getLieu_partie();?>" class="validate"  />
             
            </div>

             <div class="input-field col s9">
              
              <input type="text" name = "datePartie"  class="datepicker red-text white" placeholder="<?php echo $data->getDate_partie_heure();?>" value="<?php echo $data->getDate_partie_heure();?>"name="datePartie">
              
            </div>

            <div class="input-field col s9">

              <input name = "heurePartie" placeholder="Heure"   title ="Modifier date de la partie" class="validate"  />
              
            </div>

             <div class="input-field col s9" >
                  <select name ="equipe2" >
                    <option value="<?php echo $EquipeInvite->getNom_equipe(); ?>" > <?php echo $EquipeInvite->getNom_equipe(); ?> </option>
                    <?php 
                    $i=1;
                    foreach($Equipes as $eqp)
                    {
                    
                      if ($eqp!=null and $eqp!=$EquipeInvite)
                      {

                        ?>
                        
                          <?php echo "<option value = '" .  $eqp->getNom_equipe()."'>";?> <?php echo  $i. ' - ' .$eqp->getNom_equipe() ." </option>";?>
                        <?php 
                          $i++;
                      }
                     
                    }
                    
                    ?>
                  </select>
                </div>
              <label for="Equipe 2">Vous jouez contre quel équipe?</label>

             <div class="input-field col s9">

              <input type="hidden" name = "id_partie" value ='<?php echo $data->getId_partie(); ?>' />
              <input name = "messageGerant" placeholder="Message du gérant"   title ="Modifier le message de la partie" class="validate"  />
              
            </div>






            <div class ="col s12">
              <div class="col s5 left" style="margin-left: 5px;">
              <button type='submit' name='action' value='enregistrermodifpartie' class='col s12 btn btn-medium waves-effect waves-light ' >Enregistrer
              </button>
            </div>
                
            <div class="col s5 left" style="margin-left: 5px;">
              <button type='submit' method='post' name='action' value='afficherparties' class='col s12 btn btn-medium waves-effect waves-light orange ' >ANNULER</button>
            </div>
              
            </div>

          

           </form> 
          </span>
        </div>
      </div>
    </div>
  </div>
</div>