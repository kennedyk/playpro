</br></br>
<div class="row" style="position: center; margin-left: 50px; margin-right: 50px;">
    <div class="col s12 m12">
      <div class="card cyan darken-3">
        <div class="card-content black-text center">

          <span class="card-title ">Comment ?</span>
          <p>Nous allons vous expliquer les grandes étapes pour commencer.</br>
          Après, le reste est facile à découvrir : </br>
  Avec PlayPro, pratiquez votre sport avec professionalisme.</p>
</br>
  <span class="card-title ">Étapes 1</span>
  <p> Commencez par créer votre compte en cliquant sur le bouton ici-bas.</p>
</br>
    <span class="card-title ">Étapes 2</span>
  <p> Vous pouvez soit joindre une partie qui existe déjà, soit rejoindre une équipe existante ou rester libre.</p>
</br>
    <span class="card-title ">Étapes 3</span>
  <p> Vous pouvez créer votre propre équipe et inviter vos amis à s'y joindre.</p>
</br>
    <span class="card-title ">Étapes 4</span>
  <p> Après avoir créer votre propre équipe, vous pouvez créer des parties. Ces parties peuvent être contre des équipes qui existent déjà sur la plateforme 
  ou si vous désirez lancer un défi; il faut juste choisir invité dans l'équipe adversaire. Invité est l'équipe host de la plateforme qui ne représente aucune équipe.</p>

</br></br></br>
<span class="card-title ">Amusez-vous bien et n'oubliez pas de demander à vos amis de venir découvrir PlayPro et de vous vous partager les défis à venir.</span>

        </div>
        <div class="card-action center">
          <a  class="modal-trigger"  href="#signinmodal">Créer un compte</a>
        </div>
      </div>
    </div>


  </div>





