

  <!-- Modal Structure -->
  <div id="signinmodal1" class="modal">
    <div class="modal-content " >

          <form class="col s12" method="post">
            <div class="row">
              <form class="col s12">
               <div class="row">
                <div class="input-field col s5 left" style="margin-left: 35px;">
                  <input required id="pseudonyme" name = "pseudonyme" type="text" class="validate" pattern="([A-Za-z]+[-_]*[0-9]*){5,10}">
                  <label for="pseudonyme">Pseudonyme</label>
                </div>
                <div class="input-field col s3">
                  <select name="sexe" class="validate" required>
                    <option value="" disabled selected>Sexe</option>
                    <option value="Femme">Femme</option>
                    <option value="Homme">Homme</option>
                  </select>
                </div>
                <div class="input-field col s3">
                  <select multiple name="sport" class="validate" required>
                    <option value="" disabled selected>Sport préféré</option>
                    <option value="Soccer">Soccer</option>
                    <option value="Hockey">Hockey</option>
                    <option value="Badmington">Badminton</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s5" style="margin-left: 35px;">
                  <input id="prenom" name="prenom" type="text" class="validate">
                  <label for="prenom">Prénom</label>
                </div>
                <div class="input-field col s5 right" style="margin-right: 35px;">
                  <input id="nom" name="nom" type="text" class="validate">
                  <label for="nom">Nom</label>
                </div>
              </div>

              <div class="row">
                  <div class="input-field col s5 left" style="margin-left: 35px;">
                    <input id="email" name="courriel" type="email" class="validate" required>
                    <label for="email">Email</label>
                  </div>
                  <div class="input-field col s5 right" style="margin-right: 35px;">
                    <input id="email1" name="courriel1" type="email" class="validate" required>
                    <label for="email1">Confirmer Email</label>
                  </div>
                </div>
               

              <div class="row">
                <div class="input-field col s5 left" style="margin-left: 35px;">
                  <input id="password" name="mdp" type="password" class="validate" required>
                  <label for="password">Mot de passe</label>
                </div>
                  <div class="input-field col s5 right" style="margin-right: 35px;">
                    <input id="password1" name="mdp" type="password" class="validate" required>
                    <label for="password1">Confirmer Mot de passe</label>
                  </div>
                </div>


                  <div class="input-field col s8 right" style="margin-right: 35px;">
                    
                    <select name="type_membre" class="validate" required>
                      <option value="" disabled selected>Type membre</option>
                      <option value="admin">Administrateur</option>
                      <option value="joueur">Joueur</option>
                    </select>
                    
                  </div>

                
                </br></br>

                <div class="row">
                  <div class="col s3 left" >
                    <button type='submit' name='action' value="signin" class='col s12 btn  waves-effect waves-light ' >Créer compte</button>
                  </div>
                  <div class="col s3 right" >
                    <button type='cancel' name='btn_cancel' class='col s12 btn waves-effect orange'>Annuler</button>
                  </div>
                </div>
              </div>
            </form>
          </div>

        </form>
      </div>

    </div>

<script type="text/javascript"></script>  


@if($errors->any())
   <script type="text/javascript">

   $(document).ready(function() {

    $('#signinmodal1').modal('open');
    });

   </script>
@endif




