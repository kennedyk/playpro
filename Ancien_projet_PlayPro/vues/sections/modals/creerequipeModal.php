

  <!-- Modal Structure -->
  <div id="modalCreerEquipe" class="modal">
    <div class="modal-content " >

          <form class="col s12 center" method="post">
            <h3 class="black-text ">Création d'une équipe</h3>
            <div class="row">
              <form class="col s6 red">
               <div class="row center">
                <div class="input-field col s11" style="margin-left: 35px;">
                  <input id="nomEquipe" name = "nomEquipe" type="text" class="validate" required>
                  <label for="nomEquipe">Donne un nom de ton équipe</label>
                </div>
                
                <div class="input-field col s11" style="margin: 35px; ">
                  <select name ='sport' >
                    <option value="" disabled selected>De quel sport sera ton équipe? </option>
                    <option value="Soccer">Soccer</option>
                    <option value="Hockey">Hockey</option>
                    <option value="Badminton">Badminton</option>
                  </select>
                </div>
                <div class="input-field col s6 center" style="margin: 35px; ">
                  <select name ="nb_max_joueurs" >
                    <option value="" disabled selected>Choisis le nombre maximum de joueurs </option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>

                  </select>
                </div>
              </div>
              

                <div class="row">
                  <div class="col s3 left" >
                    <button type='submit' name='action' value="creerequipe" class='col s12 btn btn-large waves-effect waves-light ' >Créer équipe</button>
                  </div>
                  <div class="col s3 right" >
                    <button type='cancel' name='btn_cancel' class='col s12 btn btn-large waves-effect orange'>Annuler</button>
                  </div>

                </div>
              </div>
            </form>
          </div>

        </form>
      </div>

    </div>

    







