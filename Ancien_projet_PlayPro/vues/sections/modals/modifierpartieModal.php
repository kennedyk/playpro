<div id="modalModifierPartie" class="modal">
    <div class="modal-content " >

          <form class="col s12 center" method="post">
            <h3 class="black-text ">Modification d'une partie</h3>
            <div class="row">
              <form class="col s6 red">
               <div class="row center">
                
                
                <div class="input-field col s5" style="margin-left: 40px; ">
                  
                  <input placeholder="<?php echo $contenu->getSport()?>" id="typeSport" name = "typeSport" type="text" class="validate" required>
                  <label for="typeSport">Vous ne pouvez pas changer le type de sport</label>
                </div>

                <div class="input-field col s5" style="margin-left: 65px;">
                  <input placeholder="<?php echo $contenu->getLieu_partie()?>" id="lieuPartie" name = "lieuPartie" type="text" class="validate" required>
                  <label for="lieuPartie">Lieu et heure de la partie</label>
                </div>

                <div class="input-field col s5" style="margin-left: 40px; ">
                  <select name ="equipe1" >
                    <option value="" disabled > Équipe 1 </option>

                    <?php 
                    $i=1;
                    foreach($Equipes as $eqp)
                    {
                    
                      if ($eqp!=null and $eqp!=$EquipeInvite)
                      {

                    ?>
                    <option value=<?php echo $eqp->getNom_equipe(); ?>> <?php echo  $i. ' - ' .$eqp->getNom_equipe(); ?> </option>

                    <?php 
                      $i++;
                      }
                     
                    }
                    
                    ?>

                  
                  </select>
                </div>

                <div class="input-field col s5" style="margin-left: 40px; ">
                  <select name ="equipe2" >
                    <option value="<?php echo $EquipeInvite->getNom_equipe(); ?>" > <?php echo $EquipeInvite->getNom_equipe(); ?> </option>
                    <?php 
                    $i=1;
                    foreach($Equipes as $eqp)
                    {
                    
                      if ($eqp!=null and $eqp!=$EquipeInvite)
                      {

                    ?>
                    <option value=<?php echo $eqp->getNom_equipe() ?>> <?php echo  $i. ' - ' .$eqp->getNom_equipe(); ?> </option>

                    <?php 
                      $i++;
                      }
                     
                    }
                    
                    ?>
                  </select>
                </div>


                <div class ="col s12 column">
                  <div class="col s12 m6 input-field black-text">
                    <input type="text"  class="datepicker red-text white" placeholder="choisir une date" name="datePartie">
                  </div>
                  <div class="col s12 m6 ">
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="timepicker_ampm_dark">Time am/pm ( dark theme )</label>
                            <input name="tempsPartie" id="timepicker_ampm_dark" class="timepicker" type="time">
                        </div>
                    </div>
                    </div>
                  </div>
                </div>

                <div class="input-field col s11" style="margin-left: 35px;">
                  <input id="messagePartie" name = "messagePartie" type="text" class="validate" required>
                  <label for="messagePartie">Message du gérant de la partie</label>
                </div>

              </div>
              

                <div class="row">
                  <div class="col s3 left" style="margin-left: 190px;">
                    <button type='submit' name='action' value="creerpartie" class='col s12 btn btn-large waves-effect waves-light ' >Créer partie</button>
                  </div>
                  <div class="col s3 right" style="margin-right: 190px;">
                    <button type='cancel' name='btn_cancel' class='col s12 btn btn-large waves-effect orange'>Annuler</button>
                  </div>
                </div>

            </form>
          </div>

        </form>
      </div>

    </div>