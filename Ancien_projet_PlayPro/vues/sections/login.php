

<!-- se connecter -->



<a class="waves-effect waves-light btn modal-trigger" href="#modal">Se connecter</a>


<div id="modal" class="modal">
  <div class="modal-content">
    <form action ="" class="col s12" method="post">
      <div class="row">
        <form class="col s12">
         <div class="row">
          <div class="input-field col s5 left" style="margin-left: 35px;">
            <i class="material-icons prefix">account_circle</i>
            <input for="username" type="text" class="validate">
            <label name="username">Pseudonyme</label>
          </div>



          <div class="input-field col s5 right" style="margin-right: 20px;">
            <input id="password" type="password" class="validate">
            <label for="password">Mot de passe</label>
          </div>

        </div>


        <div class="row">
          <div class="col s3 left" style="margin-left: 190px;">
            <button  type='submit' name='action' class='col s12 btn btn-large waves-effect waves-light ' >Se connecter</button>
          </div>
          
          <div class="col s3 right" style="margin-right: 190px;">
            <button type='cancel' name='btn_cancel' class='col s12 btn btn-large waves-effect orange'>Annuler</button>
          </div>
        </div>

        <div class="row">


          <div class="col s3" style="margin-left: 350px">
            <label class="pink-text">Vous n'avez pas encore de compte ?</label>
            <button href="#modal" class='col s12 btn btn-large waves-effect indigo modal-trigger'>Créer Compte</button>
            

          </div>
        </div>

      </div>
    </form>
  </div>

</form>
</div>
</div>

<?php
 
  if (!ISSET($_SESSION)) session_start();
  if (ISSET($_SESSION["connected"]))
  {
?>
      <li><a href="?action=afficher">Liste des équipes</a></li>
      <li><a href="?action=accueil">Se déconnecter (<?=$_SESSION["connected"]?>)</a></li>
<?php 
  }
  else
  {
?>
      <li><a href="?action=accueil">Se connecter</a></li>
<?php 
  }
?>  




<!-- créer compte -->
<a class="waves-effect waves-light btn modal-trigger" href="#signinmodal">Créer compte</a>

<div id="signinmodal" class="modal">
    <div class="modal-content " >

          <form class="col s12" method="post">
            <div class="row">
              <form class="col s12">
               <div class="row">
                <div class="input-field col s5 left" style="margin-left: 35px;">
                  <input id="pseudonyme" type="text" class="validate">
                  <label for="pseudonyme">Pseudonyme</label>
                </div>
                <div class="input-field col s3">
                  <select>
                    <option value="" disabled selected>Sexe</option>
                    <option value="1">Femme</option>
                    <option value="2">Homme</option>
                  </select>
                </div>
                <div class="input-field col s3">
                  <select multiple>
                    <option value="" disabled selected>Sport préféré</option>
                    <option value="1">Soccer</option>
                    <option value="2">Hockey</option>
                    <option value="3">Badminton</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s5" style="margin-left: 35px;">
                  <input id="prenom" type="text" class="validate">
                  <label for="prenom">Prénom</label>
                </div>
                <div class="input-field col s5 right" style="margin-right: 35px;">
                  <input id="nom" type="text" class="validate">
                  <label for="nom">Nom</label>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s5 left" style="margin-left: 35px;">
                  <input id="password" type="password" class="validate">
                  <label for="password">Mot de passe</label>
                </div>
                  <div class="input-field col s5 right" style="margin-right: 35px;">
                    <input id="password1" type="password" class="validate">
                    <label for="password1">Confirmer Mot de passe</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s5 left" style="margin-left: 35px;">
                    <input id="email" type="email" class="validate">
                    <label for="email">Email</label>
                  </div>
                  <div class="input-field col s5 right" style="margin-right: 35px;">
                    <input id="email1" type="email" class="validate">
                    <label for="email1">Confirmer Email</label>
                  </div>
                </div>

                <div class="row">
                  <div class="col s3 left" style="margin-left: 190px;">
                    <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect waves-light ' >Créer compte</button>
                  </div>
                  <div class="col s3 right" style="margin-right: 190px;">
                    <button type='cancel' name='btn_cancel' class='col s12 btn btn-large waves-effect orange'>Annuler</button>
                  </div>
                </div>
              </div>
            </form>
          </div>

        </form>
      </div>

    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
  $(document).ready(function(){
   $('.modal-content').leanModal();
 });

</script>