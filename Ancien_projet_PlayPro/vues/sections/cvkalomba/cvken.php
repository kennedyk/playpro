<!DOCTYPE html>
 
<html>
    <head>
        <meta charset ="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" 
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel ="stylesheet "  href = "styles.css">
        <title>MON CV </title>
        <style>
        body  {
        
        }
        </style>
    </head>
    <body>
        <div id="entete">
            
            <div id="identite">
                <div id="photo" class="col-3 col-s-3 menu">
                <img src="images/136.jpg" width="250px" height="151px"  >
                </div>
                <div id ="nomAdresse" class="col-3 col-s-3 menu">
                <a id ="name">Kennedy KALOMBA</a>
                <p>4723A rue Victorin, Saint-Léonard, QC. H1R 3E2</br>
                Courriel: <a href ="mailto:kennedymukendi28@yahoo.fr" title ="Écrivez-moi"> kennedymukendi28@yahoo.fr </a></br>
                Tel: <a href ="tel:514-723-5186" title ="Appelez-moi"> 514-723-5186 </a></p>
                </div>
                
            </div>
            
        </div>
        <nav>
            <ul id="titres">
                <li><a href = "../../../index.php" title="Retourner à playpro.ca">PlayPro</a></li>
                <li><a href = "mn cv3langues.php">Langues parlées et écrites</a></li>
                <li><a href = "mn cv3experience.php">Expérience de travail</a></li>
                <li><a href = "mn cv3autre experience.php">Autre expérience</a></li>
                <li><a href = "mn cv3qualite.php">Principales Qualités</a></li>
                <li><a href = "#etudes">Études</a></li>
                <li><a href = "mn cv3connaissances.php">Connaissances informatiques <div id="fleche">&#9662;</div></a>
                    <ul id="informatique">
                        <li><a href="#">Système d’exploitation</a></li> 
                        <li><a href="#">Outils multimédias</a></li>
                        <li><a href="#">Gestion de projet</a></li>
                        <li><a href="#">Outils de gestion de versions</a></li>
                        <li><a href="#">Système de gestion de base de données</a></li>
                        <li><a href="#">Langage de programmation</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <hr>
        <hr>
                
            <ul>
            <div id="langues"><h2>Langues parlées et écrites</h2>
                <ul>
                    <li>Français et bonne maitrise de l’anglais</li>
                </ul>
            </div>
            <hr>
            <div ><h2 align="center" style="border:3px solid red margin:100px 10000px">Expérience de travail </h2>
                <ul>
                <li>Service à la clientèle <a href="http://www.ultramar.ca" target ="_blank" title ="Visiter leur site">Ultramar</a>
                    <div class = "period"> Février 2010 - Mai 2011</div></li>
                <li>Service à la clientèle Petro-Canada <div class = "period" >Janvier 2010 - Août 2016</div></li>
                <li>Assistant gérant Petro-Canada	<div class = "period" align = "right">Août 2016 - En cours</div></li>
                
                </ul>
            </div>
            <hr>
            
            <div><h2>Autre expérience</h2>
                <ul>
                    <li>Tuteur en français et en math au Collège de Rosemont<div class = "period" align="right"> Hiver 2017- En cours</div></li>
                </ul>
            </div>
            <hr>
            
            <div><h2>Principales Qualités</h2>
                <ul>
                    <li>Intervention professionnelle et efficace auprès de la clientèle</li>
                    <li>Facilité dans la gestion des situations délicates</li>
                    <li>Facilité à travailler sous la pression, avec des délais et en surtâche</li>
                    <li>Rapidité dans l’apprentissage de nouvelles choses</li>
                    <li>Capacité à transmettre mes acquis à d’autres personnes</li>
                </ul>
            </div>
            <hr>
            <a href="../../../index.php" title="Retourner à playpro.ca"><img id="home"  src="images/home.jpg"/><a>
            
            
            
            <div id="etudes" >
            <!-- <i class="fas fa-clock"></i> -->
            <h2>Études</h2> 
            
                <ul id="faites">
                <li>Diplôme d’études collégiales en informatique 
                de gestion au Cégep de Rosemont (Montréal / Québec)<div class = "period" align="right">	2016 - En cours</div></li>
                <li>Diplôme d’études secondaires à l’Institut 
                Ndinga Mbote (Kinshasa/ RD Congo)	<div class = "period" align="right">Obtenu en 2005</div></li>

                </ul>
            </div>
            <hr>
            <div><h2>Connaissances informatiques</h2>
                <ul>
                <li>Langage de programmation :</li>
                    <ul>
                    <li>Orienté objet : Python, C#, Java, VB.net, C++ </li>
                    <li>Web: Html, Css, Javascript, php</li>
                    </ul>
                <li>Système de gestion de base de données : </li>
                    <ul>
                    <li>Oracle : MySQL</li>
                    <li>Microsoft SQL Server</li>
                    </ul>
                <li>Outils de gestion de versions : </li>
                    <ul>
                    <li>Git</li>
                    <li>SVN</li>

                    </ul>
                <li>Système d’exploitation, outils multimédias et de gestion de projet :</li>
                    <ul>
                    <li>Windows (7, 8, 8.1, 10)</li>
                    <li>Unix (Ubuntu, Lubuntu, Debian, …)</li>
                    <li>Word, Excel, Access, Power Point</li>
                    <li>Adobe Photoshop et Illustrator</li>
                    <li>Office MS projet</li>
                    </ul>
                </ul>
                </div>
            
            <hr>
            </ul>
        <footer>
            <nav id="reseauxSociaux">
            <p><a href="https://www.facebook.com/ken.nedy.904"><img src="images/facebook.png" alt="Facebook" title="Facebook"/></a>
            <img src="images/twitter.png" alt="Twitter" title="Twitter" /><img src="images/rsz_linkedin.jpg" alt="Vimeo" title="Linkedin" /><img src="images/flickr.png" alt="Flickr" /><img src="images/rss.png" alt="RSS" /></p>
            </nav>
        </footer>
            
    </body>
</html>
    
  