<html>
<head>
	<title>CV DILALI YOUNES</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="nwstyle1.css">

</head>

<body>
	<header>
		<nav>
			<ul>
				<li><a href="../../../index.php">PlayPro</a></li>
				<li><a>Formation</a>
					<ul>
						<li><a href="#encours">Canada</a></li>
						<li><a href="#acheve">Maroc</a></li>

					</ul>
				</li>

				<li><a>Expériences</a>
					<ul>
						<li><a href="#travail">Travail</a></li>
						<li><a href="#stage">Stages</a></li>
					</ul>
				</li>

				<li><a href="#loisirs">Intérêts et loisirs</a></li>
			</ul>
		</nav>
	</header>



	<div id="section-left">
		<div class="section intro">

			<div class="circle">
				<img src="head01.png">				
			</div>


			<!-- <div class="logo"></div> -->
			<h1 id="nom">Younes Dilali</h1>
			<div class="content">
				<span class="grad">30 MAI 2020</span> <br>
				<span class="intro">Analyste-Développeur</span> <br>
				<span class="title">YLAB</span></br></br></br></br></br></br></br></br></br></br></br></br></br></br>
			</div>


			<h1>Mes compétences</h1></br></br></br>

			<p>HTML/CSS</p>
			<div class="container">
				<div class="skills html">85%</div>
			</div></br>

			<p>PYTHON</p>
			<div class="container">
				<div class="skills python">80%</div>
			</div></br>

			<p>C++</p>
			<div class="container">
				<div class="skills c">75%</div>
			</div></br>

			<p>JAVA</p>
			<div class="container">
				<div class="skills java">90%</div>
			</div></br>

			<p>MySQL</p>
			<div class="container">
				<div class="skills sql">82%</div>
			</div></br>

			<p>PHP</p>
			<div class="container">
				<div class="skills php">90%</div>
			</div></br>

			<p>PARADYGM</p>
			<div class="container">
				<div class="skills paradygm">85%</div>
			</div></br>

			<hr>

			
			<div id="contact">
				<h1>Contact</h1>
				<span class="num">Celullaire: 5147424645</span>

				<div class="email">
					<i class="fa fa-envelope-open-o" aria-hidden="true"></i>
					<a href="mailto:y.dilali@gmail.com">y.dilali@gmail.com</a>
				</div>
				<div class="web">
					<i class="fa fa-link" aria-hidden="true"></i>
					<a href="https://dilali.com">www.dilali.com</a>
				</div>
			</div>
		</div>
	</div>


	<div id="section-right">
		<div class="partie">

			<div class="icon-bar">
				<a href="https:\\facebook.com" class="facebook"><i class="fa fa-facebook"></i></a> 
				<a href="https:\\twitter.com" class="twitter"><i class="fa fa-twitter"></i></a> 
				<a href="https:\\google.com" class="google"><i class="fa fa-google"></i></a> 
				<a href="https:\\linkedin.com" class="linkedin"><i class="fa fa-linkedin"></i></a>
				<a href="https:\\youtube.com" class="youtube"><i class="fa fa-youtube"></i></a> 
			</div>

			<div class="section">
				<div class="title" id="home">
					<i class="fa fa-user" aria-hidden="true"></i>
					Profil
				</div>
				<p>Ayant un profil d'ingénieur financier, j'ai travaillé dans une banque en tant que chef de service pendant huit ans. Un jour j'ai décidé de refaire ma carrière vers un domaine qui m'a tant passioné, l'informatique de gestion. Aujourd'hui, en tant qu'analyste-développeur, je suis à la recherche de contrats en freelance.</p>
			</div>

			<div class="section">
				<div class="title" id="travail">
					<i class="fa fa-pencil" aria-hidden="true"></i>
					Expérience professionnelle
				</div>


				<div class="partie1">
				</br>
				<li> 2 ans d’expérience dans l’exécution des différentes tâches liées à l’activité de Ikea</li>
				<li> 8 ans d’expérience en administration</li>
				<li>Bonne réputation pour gérer avec tact les dossiers délicats et confidentiels</li>
				<li>Capacité à travailler sous pression et à gérer plusieurs tâches à la fois avec efficacité</li>


			</div>

			<div id="partie2">
				<li> Habileté à établir des contacts professionnels de collaboration</li>
				<li>    Habileté à travailler dans un environnement informatisé</li>
				<li>    Polyvalent, persévérant et organisé</li>
				<li>    Maitrise de la suite Microsoft Office (Excel, Word, Outlook, etc.) </li>
				<li>    Gestion des machines virtuelles sous OS, Windows et Linux</li>
				<li>    Connaissance des langages de programmation (Python, C++, Visual Basic, …)</li>
				<li>    Connaissances techniques des différents matériaux informatiques</li>
			</div>
		</br>
		<hr>


		<div class="content">
			<h2>Administrateur en logistique</h2>
			<h3>IKEA-BROSSARD | MAI 2018 &raquo; Aujourd'hui</h3>
			<ul class="test">
				<li>Gestion des commandes clients</li>
				<li>Plannification des commandes</li>
				<li>Assister les autres centre de distribution</li>
				<li>Gérer le flux de l'entrepôt</li>
			</ul>
		</div>

		<div class="content">
			<h2>Commis d’entrepôt</h2>
			<h3>IKEA-BROSSARD | MAI 2017 &raquo; AVRIL 2018</h3>
			<ul class="test">
				<li>Assurer l’expédition, la réception, la cueillette à la pièce, le placage et l’emballage</li>
				<li>Manipuler la marchandise entrante/sortante</li>
				<li>Faire des rapports hebdomadaires à mes supérieurs</li>
				<li>Effectuer toutes les tâches selon les différents protocoles</li>
				<li>Travailler selon les standards de qualité et de productivité établis</li>
			</ul>
		</div>

		<div class="content">
			<h2>Tuteur en français</h2>
			<h3>Collège de ROSEMONT | JANVIER 2017 &raquo; Aujourd'hui</h3>
			<ul class="test">
				<li>Enseigner le français</li>
				<li>Accompagner les étudiants du collège</li>
				<li>Faire des rapports hebdomadaires à mes supérieurs</li>
			</ul>
		</div>

		<div class="content">
			<h2>Vice président du club entrepreneurs étudiants </h2>
			<h3>Collège de ROSEMONT | JAN 2017 &raquo; MAI 2017</h3>
			<ul class="test">
				<li>Soutenir le président dans l’organisation et la répartition des tâches, par la tenue et le suivi d’un échéancier</li>
				<li>S’assure que chaque vice-président de comité soit à jour dans ses tâches</li>
			</ul>
		</div>

		<div class="content">
			<h2>Secrétaire général</h2>
			<h3>Association Al-Arabiya | SEP 2015 &raquo; JAN 2016</h3>
			<ul class="test">
				<li>Établir les stratégies pour les différents projets de l’association</li>
				<li>Gérer la communication entre les différentes entités</li>
			</ul>
		</div>

		<div class="content">
			<h2>Agent de service à la clientèle</h2>
			<h3>ATELKA | SEP 2015 &raquo; JAN 2016</h3>
			<ul class="test">
				<li>Agent de service à la clientèle</li>
				<li>Fidéliser les clients de FIDO</li>
				<li>Vendre les services FIDO « SANS FIL »</li>
			</ul>
		</div>



		<div class="content">
			<h2>Fondateur/ Directeur Administratif et Financier </h2>
			<h3>THANAW SARL | 2014 &raquo; 2015</h3>
			<ul class="test">
				<li>Déterminer les objectifs de la société</li>
				<li>Gérer la relation clientèle</li>
				<li>Gérer la comptabilité de la société</li>
				<li>Faire le suivi du processus technique et commercial</li>
				<li>Définir et gérer les actions e-marketing</li>
			</ul>
		</div>


		<div class="content">
			<h2>Chef de service</h2>
			<h3>BMCEBANK | MAR 2008 &raquo; AOUT 2015</h3>
			<ul class="test">
				<li>Gérer et développer le porte feuille clients</li>
				<li>Étudier, analyser et valider les dossiers de crédits</li>
				<li>Veiller sur la gestion et le suivi de l’équipe</li>
				<li>Analyser et développer le produit net bancaire de l’agence</li>
				<li>Faire de la prospection</li>
				<li>Établir des plans d’action afin d’atteindre les objectifs assignés</li>
			</ul>
		</div>

		<div class="content">
			<h2>Gestionnaire de porte feuille clientèle </h2>
			<h3>Marketing Call Center | SEP 2007 &raquo; FEV 2008</h3>
			<ul class="test">
				<li>Développer le portefeuille de l’équipe par téléphone</li>
				<li>Assigner des objectifs pour l’équipe de vente dans le secteur de la parapharmacie</li>
			</ul>
		</div>

		<div class="content">
			<h2>Chargé de clientèle </h2>
			<h3>WEBHELP | JAN 2007 &raquo; MAI 2007</h3>
			<ul class="test">
				<li>Conseiller technique : Assistance technique dans le projet « CLUB INTERNET »</li>
				<li>Conseiller commercial : Contacter les clients de « Banque Accord » afin de leur</li>
				<li>proposer le rachat de leurs crédits avec un taux préférentiel.</li>
			</ul>
		</div>


		<div class="title" id="stage">
			<i class="fa fa-pencil" aria-hidden="true"></i>
			Stages
		</div>

		<div class="content">
			<h2>Stagiaire dans le département marketing </h2>
			<h3>COCA-COLA | Février 2007</h3>
			<ul class="test">
				<li >Analyse des réalisations hebdomadaires</li>
				<li>Parcourir la zone commerciale</li>
			</ul>
		</div>

		<div class="content">
			<h2>Stagiaire dans le département économique </h2>
			<h3>OCP | SEP 2006</h3>
			<ul class="test">
				<li>Analyser le bilan des rentrées relatives à SAFI</li>
				<li>Etablir des fiches Excel explicatives au responsable</li>
			</ul>
		</div>

		<div class="content">
			<h2>Stagiaire dans le service de dédouanement </h2>
			<h3>Administration douanière des impôts indirects | Juillet 2006</h3>
			<ul class="test">
				<li>Analyser les dossiers d’import/export </li>
				<li>Faire des tournées avec les douaniers dans le cadre du contrôle des conteneurs</li>
			</ul>
		</div>

		<div class="content">
			<h2>Stagiaire dans le département du commerce et des relations internationales </h2>
			<h3>BANK AL MAGHREB | Juillet 2005</h3>
			<ul class="test">
				<li>Stage de découverte </li>
				<li>Initiation aux opérations d’analyse et de veilles internationales</li>
			</ul>
		</div>

		<div class="content">
			<h2>Stagiaire dans une agence bancaire </h2>
			<h3>Crédit Agricole | Août 2005</h3>
			<ul class="test">
				<li>Initiation aux opérations courantes de la banque </li>
				<li>Entrer en relation avec les clients de l’agence</li>
				<li>Composition des dossiers de crédits</li>
			</ul>
		</div>



	</div>

	<div class="section">
		<div class="title">
			<i class="fa fa-book" aria-hidden="true"></i>
			Formation
		</div>


		<div class="content" id="encours">
			<h2>DEC au Collège de Rosemont</h2>
			<h3>Collège de Rosemont | AUG 2016 &raquo; En cours</h3>
			<ul class="test">
				<li>Informatique de gestion Maitrise</li>
			</ul>
		</div>

		<div class="content" id="acheve">
			<h2>Maitrise</h2>
			<h3>École Marocaine de Banque et de Commerce International | 2009</h3>
			<ul class="test">
				<li>Option Finance - Banque</li>
			</ul>
		</div>

		<div class="content">
			<h2>Bachelor</h2>
			<h3>École Marocaine de Banque et de Commerce International | 2004 &raquo; 2008</h3>
			<ul class="test">
				<li>Option COMMERCE INTERNATIONAL (Cambisme international)</li>
			</ul>
		</div>

		<div class="content">
			<h2>DES</h2>
			<h3>Lycée Inm Rochd | 2001 &raquo; 2004</h3>
			<ul class="test">
				<li>Informatique de gestion Maitrise</li>
			</ul>
		</div>
	</div>

	<div class="section">
		<div class="title">
			<i class="fa fa-book" aria-hidden="true"></i>
			Compétences
		</div>
		<ul>
			<li>Programming</li>
			<li>Photo Editing</li>
			<li>Project Management</li>
		</ul>
	</div>

	<div class="section">
		<div class="title" >
			<i class="fa fa-trophy" aria-hidden="true"></i>
			Loisirs et Intérêts
		</div>
		<div class="content">
			<li id="loisirs">La lecture, la musique</li>
			<li>Badminton, Les échecs </li>
			<li>Jeux de stratégies</li>
		</div>
	</div>
</div>
</div>

</body>
</html>