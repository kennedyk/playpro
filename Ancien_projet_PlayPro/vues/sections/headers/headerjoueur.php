
<!-- navbar -->

<header>


  <nav class="nav-wraper blue" id="entete">

    <div class="container ">



      <a href="?action=portail" title="Retourne au portail" class="brand-logo center"><img id="logo" src="vues/sections/images/logo.png"/></a> 


      <ul id ="navbar1" id="nav-mobile" class="left hide-on-med-and-down">
        <li><a id="navicon" class=' btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-futbol"></i></a></li>

        <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-baseball-ball"></i></a></li>

        <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-basketball-ball"></i></a></li>

        <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-football-ball"></i></a></li>

        <li><a id="navicon" class=' btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-hockey-puck"></i></a></li>
      </ul>
      



      <ul id ="navbar1" id="nav-mobile" class="right hide-on-med-and-down">
       <li><a id="navicon" class=' btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-volleyball-ball"></i></a></li>
       <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-table-tennis"></i></a></li>
       <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-bowling-ball"></i></a></li>
       <li><a id="navicon" class=' btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-golf-ball"></i></a></li>
       <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-bullhorn"></i></a></li>

     </ul>

   </div>

 </nav>


 <ul id='dropdown2' class='dropdown-content'>
  <li><a href="?action=afficherparties"><span class="blue-text text-darken-2">Voir toutes les parties</span></a></li>
  <li><a class="modal-trigger" href="#modalCreerPartie"><span class="blue-text text-darken-2">Créer ta propre partie</span></a></li>
  <li class="divider" tabindex="-1"></li>


</ul>

<ul id='dropdown3' class="dropdown-content">
  <li><a class="blue-text text-darken-2 menu" href="?action=afficherequipes"><span class="blue-text text-darken-2">Voir toutes les équipes</span></a></li>
  <li><a class="modal-trigger" href="#modalCreerEquipe"><span class="blue-text text-darken-2">Créer ta propre équipe</span></a></li>
  <li class="divider" tabindex="-1"></li>
</ul>

<ul id="dropdown4" class="dropdown-content">
  <li><a class="modal-trigger" href="#signinmodal1"><span class="blue-text text-darken-2">Créer un membre</span></a></li>
  <li><a class="" href="?action=affichermembres"><span class="blue-text text-darken-2">Voir tous les membres</span></a></li>
  <li class="divider" tabindex="-1"></li>


</ul>


<ul class="sidenav" id="mobile-links">
  <li><a class='btn blue' href="?action=accueil"><i class="material-icons">home</i>Accueil</a></li>
  <li><a class='dropdown-trigger btn blue' href='#' data-target='dropdown5'><i class="material-icons">directions_run</i>Les équipes</a></li>
  <ul id='dropdown5' class="dropdown-content ">
   <li><a class="blue-text text-darken-2 menu" href="?action=afficherequipes" >Voir toutes les équipes</a></li>
   <li><a class="modal-trigger" href="#modalCreerEquipe" class="blue-text text-darken-2">Créer ta propre équipe</a></li>
 </ul>
 <li><a class='dropdown-trigger btn blue' href='#' data-target='dropdown6'><i class="material-icons">flag</i>Les parties</a></li>
 <ul id='dropdown6' class='dropdown-content'>
  <li><a href="?action=afficherparties" class="blue-text text-darken-2 menu">Voir toutes les parties</a></li>
  <li><a class="modal-trigger" href="#modalCreerPartie"class="blue-text text-darken-2">Créer ta propre partie</a></li>
</ul>
<li><a class='btn blue' href="?action=profil"><i class="material-icons">face</i>Profil</a></li>
<li><a class="btn blue " href="?action=logout" > <i class="material-icons">exit_to_app</i>Quitter</a></li>
</ul>

<ul class="sidenav" id="mobile-links-sport">
  <li><a class="btn light-green lighten-2"><i class="fas fa-volleyball-ball" ></i>&nbsp;&nbsp;&nbsp;&nbsp;Volleyball</a></li>
  <li><a class="btn brown lighten-3" ><i class="fas fa-football-ball"></i>&nbsp;&nbsp;&nbsp;&nbsp;Football</a></li>
  <li><a class="btn green" ><i class="fas fa-table-tennis"></i>&nbsp;&nbsp;&nbsp;&nbsp;Tennis</a></li>
  <li><a class="btn light-blue accent-2" ><i class="fas fa-futbol"></i>&nbsp;&nbsp;&nbsp;&nbsp;Soccer</a></li>
  <li><a class='btn blue-grey lighten-4' ><i class="fas fa-bowling-ball"></i>&nbsp;&nbsp;&nbsp;&nbsp;Bowling</a></li>
  <li><a class='btn yellow darken-3' > <i class="fas fa-golf-ball"></i>&nbsp;&nbsp;&nbsp;&nbsp;Golf</a></li>
  <li><a class='btn red' > <i class="fas fa-bullhorn"></i>&nbsp;&nbsp;&nbsp;&nbsp;Notifications</a></li>
</ul>



<nav id="barre">
  <div class="nav-wrapper " >

    <div class="container">

      <a href="index.php" id ="dep-menu" class="sidenav-trigger btn-floating btn-large pulse blue darken-2" data-target="mobile-links">
        <i class="material-icons">menu</i>
      </a>

      <a id ="dep-menu" class="sidenav-trigger btn-floating btn-large pulse cyan darken-3" data-target="mobile-links-sport" style=" margin-left: 15px;">
        <i class="material-icons">directions_walk</i>
      </a>

      <ul id="nav-mobile" class="left hide-on-med-and-down">
        <li><a class=' btn blue z-depth-5 btn-floating pulse' href='?action=accueil' > <i class="material-icons">home</i></a></li>
        <?php 
        $udao = new MembreDAO();
        $user = $udao->findBytitre($_SESSION['connected']);

        
        if(!is_null($user)){
            if ($user->getType_membre() ==="admin"){
                ?>
                <li><a class='dropdown-trigger ' href='#dropdown4' data-target='dropdown4'> Les Membres</a></li>

                <?php

              }

        }

        ?>

        <li><a class='dropdown-trigger ' href='#dropdown2' data-target='dropdown2'> Les parties</a></li>


      </ul>

      <ul id="nav-mobile" class="right hide-on-med-and-down">


        <li>
          <a class='dropdown-trigger ' href='#' data-target='dropdown3'> Les équipes</a>
        </li>
        <li>
          <a href="?action=profil">Profil</a>
        </li>
        <li>
          <a class="modal-trigger btn blue z-depth-5 btn-floating pulse" href="?action=logout" > <i class="material-icons">exit_to_app</i></a>
        </li>
        
      </ul>
      <?php include("./vues/sections/modals/SignInModal1.php");?>
      <?php include("./vues/sections/modals/creerequipeModal.php");?>
      <?php include("./vues/sections/modals/creerpartieModal.php");?>

    </div>

    <!-- </div> -->
  </div>
</nav>


<!-- fin navbar -->
<?php
if (ISSET($_SESSION["connected"]))
{
  ?>

  <nav id="fil">
    <div class="nav-wrapper " >
      <div class="col s12">
        <a href="#!" class="breadcrumb"><a class="left">Bonjour <?php echo $_SESSION['connected']  ?></a> <a class="right" href="">Date : <?php echo date("Y-m-d");  ?></a> </a>

      </div>
    </div>
  </nav>

<?php }

?>



</header>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>

  $('.dropdown-trigger').dropdown();

  $(document).ready(function(){
    $('.modal-trigger').leanModal();
  });

</script>
