<!-- navbar -->

<header>
  <nav class="nav-wraper blue" id="entete">

    <div class="container ">

      <a href="index.php" class="brand-logo center"><img id="logo" src="vues/sections/images/logo.png"/></a> 

      <ul id ="navbar1" id="nav-mobile" class="left hide-on-med-and-down">
        <li><a id="navicon" class=' btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-futbol"></i></a></li>

        <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-baseball-ball"></i></a></li>

        <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-basketball-ball"></i></a></li>

        <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-football-ball"></i></a></li>

        <li><a id="navicon" class=' btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-hockey-puck"></i></a></li>
      </ul>
      



      <ul id ="navbar1" id="nav-mobile" class="right hide-on-med-and-down">
       <li><a id="navicon" class=' btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-volleyball-ball"></i></a></li>
       <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-table-tennis"></i></a></li>
       <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-bowling-ball"></i></a></li>
       <li><a id="navicon" class=' btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-golf-ball"></i></a></li>
       <li><a id="navicon" class='btn blue z-depth-5 btn-floating pulse' > <i class="fas fa-bullhorn"></i></a></li>

     </ul>

   </div>

 </nav>


 <ul class="sidenav" id="mobile-links">
  <li><a class='dropdown-trigger btn lightblue' href="#" data-target='dropdown11'><i class="material-icons">person_add</i>Votre compte</a></li>
  <ul id='dropdown11' class="dropdown-content">
   <li><a href="#modal2" class="modal-trigger blue-text text-darken-2">Se connecter</a></li>
   <li><a href="#signinmodal" class="modal-trigger blue-text text-darken-2">Créer un compte</a></li>
 </ul>
 <li><a class='btn blue' href="?action=accueil"><i class="material-icons">home</i>Accueil</a></li>
 <li><a class='btn blue' href="?action=quoi"><i class="material-icons">live_help</i>Quoi?</a></li>
 <li><a class='btn blue' href="?action=comment"><i class="material-icons">local_library</i>Comment?</a></li>

 <li><a class='btn blue' href="?action=apropos"><i class="material-icons">info</i>À propos</a></li>
 <li><a class='btn blue' href="?action=contact"><i class="material-icons">markunread</i>Contact</a></li>
</ul>

<ul class="sidenav" id="mobile-links-sport">
  <li><a class="btn light-green lighten-2"><i class="fas fa-volleyball-ball" ></i>&nbsp;&nbsp;&nbsp;&nbsp;Volleyball</a></li>
  <li><a class="btn brown lighten-3" ><i class="fas fa-football-ball"></i>&nbsp;&nbsp;&nbsp;&nbsp;Football</a></li>
  <li><a class="btn green" ><i class="fas fa-table-tennis"></i>&nbsp;&nbsp;&nbsp;&nbsp;Tennis</a></li>
  <li><a class="btn light-blue accent-2" ><i class="fas fa-futbol"></i>&nbsp;&nbsp;&nbsp;&nbsp;Soccer</a></li>
  <li><a class='btn blue-grey lighten-4' ><i class="fas fa-bowling-ball"></i>&nbsp;&nbsp;&nbsp;&nbsp;Bowling</a></li>
  <li><a class='btn yellow darken-3' > <i class="fas fa-golf-ball"></i>&nbsp;&nbsp;&nbsp;&nbsp;Golf</a></li>
  <li><a class='btn red' > <i class="fas fa-bullhorn"></i>&nbsp;&nbsp;&nbsp;&nbsp;Notifications</a></li>
</ul>


<nav id="barre">
  <div class="nav-wrapper " >

    <div class="container ">

      <a href="index.php" id ="dep-menu" class="sidenav-trigger btn-floating btn-large pulse blue darken-2" data-target="mobile-links">
        <i class="material-icons">menu</i>
      </a>

      <a id ="dep-menu" class="sidenav-trigger btn-floating btn-large pulse cyan darken-3" data-target="mobile-links-sport" style=" margin-left: 15px;">
        <i class="material-icons">directions_walk</i>
      </a>

      <ul id="nav-mobile" class="left hide-on-med-and-down">
        <li><a class=' btn blue z-depth-5 btn-floating pulse' href='?action=accueil' > <i class="material-icons">home</i></a></li>

        <li><a href='?action=quoi' > Quoi?</a></li>

        <li><a href="?action=comment">Comment?</a></li>

      </ul>

      <ul id="nav-mobile" class="right hide-on-med-and-down">


        <?php 
        
        
        if (ISSET($_REQUEST["action"])){
          if ($_REQUEST["action"] ==  "apropos"){

            echo '<li><a class="#0d47a1 blue darken-2" href="?action=apropos">À propos</a></li>';
            echo '<li><a href="?action=contact">Contact</a></li>';
            echo '<li><a class="modal-trigger btn blue z-depth-5 btn-floating pulse" href="#modal2"> <i class="material-icons">account_circle</i></a></li>';
            
          }else if ($_REQUEST["action"] == "contact"){
            echo '<li><a  href="?action=apropos">À propos</a></li>';
            echo '<li><a class="#0d47a1 blue darken-2" href="?action=contact">Contact</a></li>';
            echo '<li><a class="modal-trigger btn blue z-depth-5 btn-floating pulse" href="#modal2" > <i class="material-icons">account_circle</i></a></li>';

          }else{
            echo '<li><a href="?action=apropos">À propos</a></li>';
            echo '<li><a href="?action=contact">Contact</a></li>';
            echo '<li><a class="modal-trigger btn blue z-depth-5 btn-floating pulse" href="#modal2" > <i class="material-icons">account_circle</i></a></li>';

          }
          
        }else {

          echo '<li><a href="?action=apropos">À propos</a></li>';
          echo '<li><a href="?action=contact">Contact</a></li>';
          echo '<li><a class="modal-trigger btn blue z-depth-5 btn-floating pulse" href="#modal2" > <i class="material-icons">account_circle</i></a></li>';
        }
        ?>

        
      </ul>
      <?php include("./vues/sections/modals/logInModal.php");?>
      <?php include("./vues/sections/modals/SignInModal.php");?>
      

    </div>

    <!-- </div> -->
  </div>
</nav>


<!-- fin navbar -->



</header>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>

  $('.dropdown-trigger').dropdown();

  $('document').ready(function(){
   $('.modal-trigger').leanModal();
 });

</script>
