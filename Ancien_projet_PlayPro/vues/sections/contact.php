



</br></br></br>



<div id="index-banner" class="parallax-container valign-wrapper">
  <div class="section no-pad-bot center">
    <div class="container valign">
      <br><br>
      <h2 class="header center white-text text-darken-3">Écrivez-nous</h2>
      <div class="row center">
        <h5 class="header col s12 light white-text text-darken-3">Toujours disponible pour améliorer notre service</h5>
      </div>
      <div class="row center">
        <a href="#startPosts" class=" waves-effect waves-light white darken-3 head-link">
          <a href="#name" class="white-text">
            <i class="fas fa-envelope"></i>
          </a>
        </a>
      </div>
      <br><br>

    </div>
  </div>
  <div class="parallax"><img src="vues/sections/images/17334579098_5d88ddb37f_b.jpg" alt="Unsplashed background img 2"></div>
</div>

<div class="container" id="startPosts">
  <br>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col s12 m10 offset-m1 l9 offset-l1">

      <form class="col s8">

        <div class="row">
          <div class="input-field col s6">
            <input id="name" type="text" class="validate">
            <label for="name">Votre nom</label>
          </div>


          <div class="input-field col s6">
            <input id="email" type="email" class="validate">
            <label for="email">Votre courriel</label>

          </div>
        </div>

        <div class="row">
          <div class="input-field col s12">
            <input id="message-sub" type="email" class="validate">
            <label for="message-sub">Objet du message</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s12">
           <textarea id="text_area" class="materialize-textarea"></textarea>
           <label for="text_area">Votre message</label>
         </div>
       </div>


       <div>
        <a class="waves-effect waves-light btn blue">Envoyer Message<i class="mdi-content-send right"></i></a>
      </div>
    </form>

    <div class="col s4">
      <br>
      <a href="index.php" title="Accueil"><img id="img1" class="responsive-img" src="vues/sections/images/blueplay.png"></a>

    </div>




  </div>
</div>
</div>






<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/materialize.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
  $(document).ready(function(){
    $('.sidenav').sidenav();
    $('.slider').slider();
    $('.dropdown-trigger').dropdown();
    $('select').formSelect();

    $('.modal').modal();
  }); 







</script>

<script>
  $(document).ready(function(){
    $('.materialboxed').materialbox();
    $('.parallax').parallax();
    $('.button-collapse').sideNav();
    $('.sidenav').sidenav();
    

  });
  $('.head-link').click(function(e) {
    e.preventDefault();

    var goto = $(this).attr('href');

    $('html, body').animate({
      scrollTop: $(goto).offset().top
    }, 800);
  });

  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, {
      direction: 'bottom',
      hoverEnabled: false
    });
  });

</script>

