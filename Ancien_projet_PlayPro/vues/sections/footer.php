
<footer>
  
  
  <section class="section section-follow blue darken-2 white-text center">
    <div class="row">
      <div >
        <ul id="conditions">
          <li><a class="footer-link" href="apropos.php">À propos de PLAYPRO</a> | </li>
          <li><a class="footer-link" href="#!">Sécurité</a> | </li>
          <li><a class="footer-link" href="#!">Politique de confidentialité</a> | </li>
          <li><a class="footer-link" href="#!">Conditions d'utilisation</a>  | </li>
          <li><a class="footer-link" href="help.php">Centre d'aide</a></li>
        </ul>
      </div>
      
      <div class="container ">
        
        
        <div class="col s12" id="reseaux">
          <h4><i class="far fa-copyright"></i>PlayPro</h4>
          <p>Suivez-nous sur les réseaux sociaux pour les nouveautés</p>
          <a href="#" class="white-text">
            <i class="fab fa-facebook fa-4x"></i>
          </a>
          <a href="#" class="white-text">
            <i class="fab fa-twitter fa-4x"></i>
          </a>
          <a href="#" class="white-text">
            <i class="fab fa-linkedin fa-4x"></i>
          </a>
          <a href="#" class="white-text">
            <i class="fab fa-google-plus fa-4x"></i>
          </a>
          <a href="#" class="white-text">
            <i class="fab fa-pinterest fa-4x"></i>
          </a>
        </div>
      </div>
    </div>
  </section>
</footer>



