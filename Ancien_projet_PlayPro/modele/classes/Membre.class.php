<?php

class Membre {
    private $id;
    private $pseudo;
    private $nom;
    private $prenom;
    private $sexe;
    private $sport;
    private $courriel;
    private $date_inscription;
    private $type_membre;
    private $mdp;
    private $equipe;
    
    function __construct(array $donnees) {
        $this->hydrate($donnees);
    }
    
    function setID($value){
        $this->id = $value;
    }
    
    function setPseudo($value){
        $this->pseudo = $value;
    }
    
    function setNom($value){
        $this->nom = $value;
    }
    
    function setPrenom($value){
        $this->prenom = $value;
    }

    function setSexe($value){
        $this->sexe = $value;
    }

    function setSport($value){
        $this->sport = $value;
    }
        
    function setCourriel($value){
        $this->courriel = $value;
    }
        
    function setDate_inscription($value) {
        $this->date_inscription = $value;
    }
        
    function setType_membre($value){
        $this->type_membre = $value;

        
    }
    function setMdp($value){
        $this->mdp = $value;
    }

    function setEquipe($value){
        $this->equipe = $value;
    }
    
    function getID(){
        return $this->id;
    }

    function getPseudo(){
        return $this->pseudo;
    }
    
    function getNom(){
        return $this->nom;
    }
    
    function getPrenom(){
        return $this->prenom;
    }

    function getSexe(){
        return $this->sexe;
    }

    function getSport(){
        return $this->sport;
    }
    
    function getCourriel(){
        return $this->courriel;
    }
    
    function getDate_inscription(){
        return $this->date_inscription;
    }
    
    function getType_Membre(){
        return $this->type_membre;
    }
    
    function getMdp(){
        return $this->mdp;
    }

    function getEquipe(){
        return $this->equipe;
    }
   
    public function hydrate(array $donnees){
        foreach ($donnees as $key => $value)
           
		  	{
		    	// On récupère le nom du setter correspondant à l'attribut.
		    	$method = 'set'.ucfirst($key);
               
		        
		    	// Si le setter correspondant existe.
		    	if (method_exists($this, $method))
		    	{
		      		// On appelle le setter.
		      		$this->$method($value);

                    
		    	}
		  	}

    } 

    public function toArray(){
        $tab = array();

        if(!is_null($this->id)){
            $tab['id'] = $this->id;
        }

        if(!is_null($this->pseudo)){
            $tab['pseudo'] = $this->pseudo;
        }

        if(!is_null($this->nom)){
            $tab['nom'] = $this->nom;
        }

        if(!is_null($this->prenom)){
            $tab['prenom'] = $this->prenom;
        }

        if(!is_null($this->sexe)){
            $tab['sexe'] = $this->sexe;
        }

        if(!is_null($this->sport)){
            $tab['sport'] = $this->sport;
        }

        // if(!is_null($this->nom)){
        //     $tab['nom'] = $this->nom;
        // }

        if(!is_null($this->courriel)){
            $tab['courriel'] = $this->courriel;
        }
        if(!is_null($this->date_inscription)){
            $tab['date_inscription'] = $this->date_inscription;
        }

        if(!is_null($this->type_membre)){
            $tab['type_membre'] = $this->type_membre;
        }

        if(!is_null($this->mdp)){
            $tab['mdp'] = $this->mdp;
        }

        if(!is_null($this->equipe)){
            $tab['equipe'] = $this->equipe;
        }

        return $tab;

    }
        
    
}