<?php

class Partie {
    private $id_partie;
    private $date_partie_heure;
    private $sport;
    private $equipe_1;
    private $equipe_2;
    private $lieu_partie;
    private $score; 
    

    function __construct(array $donnees) {
        $this->hydrate($donnees);
    }
    
    function setId_partie($value){
        $this->id_partie = $value;
    }
    
    function setDate_partie_heure($value){
        $this->date_partie_heure = $value;
    }

    function setSport($value){
        $this->sport = $value;
    }
    
    function setEquipe_1($value){
        $this->equipe_1 = $value;
    }
    
    function setEquipe_2($value){
        $this->equipe_2 = $value;
    }

    function setLieu_partie($value){
        $this->lieu_partie = $value;
    }
    
    function setScore($value){
        $this->score = $value;
    }
    
    function getId_partie(){
        return $this->id_partie;
    }
      
    function getDate_partie_heure(){
        return $this->date_partie_heure;
    }

    function getSport(){
        return $this->sport;
    }
      
    function getEquipe_1(){
        return $this->equipe_1;
    }
      
    function getEquipe_2(){
        return $this->equipe_2;
    }

    function getLieu_partie(){
        return $this->lieu_partie;
    }

    function getscore(){
        return $this->score;
    } 

    public function hydrate(array $donnees){
        foreach ($donnees as $key => $value)
            {
                // On récupère le nom du setter correspondant à l'attribut.
                $method = 'set'.ucfirst($key);
                
                // Si le setter correspondant existe.
                if (method_exists($this, $method))
                {
                    // On appelle le setter.
                    $this->$method($value);
                }
            }
    }

    public function toArray(){
        $tab = array();

        if(!is_null($this->id_partie)){
            $tab['id_partie'] = $this->id_partie;
        }

        if(!is_null($this->date_partie_heure)){
            $tab['date_partie_heure'] = $this->date_partie_heure;
        }

        if(!is_null($this->sport)){
            $tab['sport'] = $this->sport;
        }

        if(!is_null($this->equipe_1)){
            $tab['equipe_1'] = $this->equipe_1;
        }

        if(!is_null($this->equipe_2)){
            $tab['equipe_2'] = $this->equipe_2;
        }

        if(!is_null($this->lieu_partie)){
            $tab['lieu_partie'] = $this->lieu_partie;
        }

        if(!is_null($this->score)){
            $tab['score'] = $this->score;
        }

        return $tab;
    }

}