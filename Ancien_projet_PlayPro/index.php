<?php

	// -- Contrôleur frontal --
	require_once('./controleur/Routeur.class.php');
	date_default_timezone_set('America/New_York');
	// echo "Today is " . date("Y-m-d") . "<br>";
	if (ISSET($_REQUEST["action"]))
		{
			//$vue = ActionBuilder::getAction($_REQUEST["action"])->execute();
			/*
			Ou :*/
			// echo $_REQUEST["action"];
			$actionDemandee = $_REQUEST["action"];
			$controleur = Routeur::getAction($actionDemandee);

			$vue = $controleur->execute();
			/**/
		}
	else	
		{
			$action = Routeur::getAction("accueil");
			$vue = $action->execute();
		}

	

	echo $vue->genererContenu();
?>
