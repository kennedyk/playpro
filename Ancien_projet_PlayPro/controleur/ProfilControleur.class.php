<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');
require_once('./modele/MembreDAO.class.php');
require_once('./classes/Message.class.php');

class ProfilControleur implements Action {
	public function execute(){
		$dao = new MembreDAO();
		if (!ISSET($_SESSION)) session_start();
		if (!ISSET($_SESSION["connected"]))	//utilisateur non connecté.
			return new Page("accueil", "Playpro - Accueil", null, null);
		if (ISSET($_REQUEST['nom'])){

			$data = $dao->findBytitre($_REQUEST["nom"]);
			return new Page("profil", "Playpro - Mon profil", $data, null);
		}


		$data = $dao->findBytitre($_SESSION['connected']);
		return new Page("profil", "Playpro - Mon profil", $data, null);
	}
}
?>