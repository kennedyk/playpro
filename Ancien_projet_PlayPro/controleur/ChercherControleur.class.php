<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');
require_once('./modele/MembreDAO.class.php');
require_once('./modele/EquipeDAO.class.php');
require_once('./classes/Message.class.php');
require_once('./modele/PartieDAO.class.php');



class ChercherequipeControleur implements Action {
	public function execute(){

		if (!ISSET($_REQUEST["nomEquipe"]))
			return new Page("accueil", "PlayPro - Accueil", null, null);
		if (!$this->valide())
		{
			//$_REQUEST["global_message"] = "Le formulaire contient des erreurs. Veuillez les corriger.";	
			return new Page("accueil", "PlayPro - Accueil", null, null);
		}
		if (!ISSET($_SESSION)) session_start();

		$data = array();
		$udao = new MembreDAO();
		$edao = new EquipeDAO();


		if(!(is_null($edao->findBytitre($_REQUEST['nomEquipe'])))){
			
			return new Page("afficher", "PlayPro - Équipe non créée", $data, "Oups!!! Ce nom d'équipe [". $_REQUEST['nomEquipe'] . "] existe déjà");
		}

		$infos = array();

		$infos['nom_equipe'] = $_REQUEST['nomEquipe'];
		// $info['sport'] = $_REQUEST['sport'];
		// $info['mdpEquipe'] = $_REQUEST[mdpEquipe];
		

		$date_element = date("Y-m-d");
		$date_str = date('Y') . "-" . date('m') . "-" . date('d');
		

		$infos['date_creation'] = $date_str;
		$infos['nb_max_joueurs'] = $_REQUEST['nb_max_joueurs'];
		$infos['sport'] = $_REQUEST['sport'];
		$infos['nb_joueurs'] = 1;

		$user = $udao->findBytitre($_SESSION["connected"]);
		$infos['capitaine'] = $user->getID();
		$infos['nb_parties_jouees'] = 0;
		
		print_r($infos);
		$cree = new Equipe($infos);
		

		


		$message = "L'équipe " . $cree->getNom_equipe() . " a bien été créée.";

		
		$cree = $edao->create($cree);
		array_push($data, $cree);

		


		if(!(is_null($edao->findBytitre($cree->getNom_equipe()))))
		{	
			return new Page("afficherequipes", "PlayPro -Équipe créée", $data, $message);

		}else{
			
			return new Page("afficherequipes", "PlayPro - Équipe non créée", null, "Oups!!! Erreur...");
		}
		
		
	}


	public function valide()
	{
		$result = true;
		if ($_REQUEST['nomEquipe'] == "")
		{
			$_REQUEST["field_messages"]["username"] = "Donnez votre nom d'utilisateur";
			$result = false;
		}	
		// if ($_REQUEST['mdpEquipe'] == "")
		// {
		// 	$_REQUEST["field_messages"]["password"] = "Mot de passe obligatoire";
		// 	$result = false;
		// }	
		return $result;
	}
}




class ChercherpartiesControleur implements Action {
	public function execute(){
		
		 

		if (!ISSET($_REQUEST["sport"]))
			return new Page("accueil", "PlayPro - Accueil", null, null);
		if (!$this->valide())
		{
			
			return new Page("accueil", "PlayPro - Accueil", null, null);
		}
		if (!ISSET($_SESSION)) session_start();

		$data = array();
		//$udao = new MembreDAO();
		$pdao = new PartieDAO();

		$infos = array();

		$infos['sport'] = $_REQUEST['sport'];

		$infos['date'] = $_REQUEST['date'];

		

		$date = date_parse($_REQUEST['date']);
	    $jour = $date['day'];
	    $mois = $date['month'];
	    $annee = $date['year'];

	    
		//echo "Mois : $mois; Jour : $jour; Année : $annee<br />\n";

		$data = $pdao->findByOrderBy($infos, null);
		
		

		$date_element = date("Y-m-d");
		$date_str = date('Y') . "-" . date('m') . "-" . date('d');		

			

		$message = "Les parties de ". $_REQUEST['sport']. " qui se joueront à partir de  " . $_REQUEST['date'] . ".";


			
		return new Page("afficherparties", "PlayPro -Partie à venir", $data, $message);

		// }else{
			
		// 	// print_r($data);
		// 	return new Page("afficherparties", "PlayPro - Partie non créée", null, "Oups!!! Erreur...");
		// }

		

	}


	public function valide()
	{
		$result = true;
		if ($_REQUEST['sport'] == "")
		{
			$_REQUEST["field_messages"]["username"] = "Précisez date et lieu de rencontre !";
			$result = false;
		}	
		
		return $result;
	}
}