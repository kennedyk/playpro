<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');
require_once('./modele/MembreDAO.class.php');
require_once('./modele/EquipeDAO.class.php');
require_once('./modele/PartieDAO.class.php');
require_once('./classes/Message.class.php');




	class ModifierequipeControleur implements Action {
	public function execute(){

		

		if (!ISSET($_REQUEST['id']))

		print_r($_REQUEST);
		
		if (!ISSET($_REQUEST["nom"]))

			
			return new Page("accueil", "PlayPro - Accueil", null, null);
		
		if (!ISSET($_SESSION)) session_start();

		
		$udao = new MembreDAO();
		$edao = new EquipeDAO();

		$user = $udao->findBytitre($_SESSION["connected"]);
		
		// $infos['nb_max_joueurs'] = $_REQUEST['nb_max'];
		// $infos['sport'] = $_REQUEST['sport'];
		$data = $edao->findBytitre($_REQUEST['nom']);

		return new Page("modifierequipe", "PlayPro - Modification", $data, null);
		

		
		$equipe = $edao->create($equipe);
		array_push($data, $equipe);
	}
}

class EnregistrermodifequipeControleur implements Action {
	public function execute(){


		if (!ISSET($_REQUEST['id']))

		
		if (!ISSET($_REQUEST["nom"]))



			return new Page("accueil", "PlayPro - Accueil", null, null);
		
		if (!ISSET($_SESSION)) session_start();
		
		
		
		$udao = new MembreDAO();
		$edao = new EquipeDAO();

		$user = $udao->findBytitre($_SESSION["connected"]);
		
		$data = $edao->findBytitre($_REQUEST['nom']);

		$data->setSport($_REQUEST['sport']);
		$data->setNb_max_joueurs($_REQUEST['nb_max']);

		echo $data->getNom_equipe();

		$reussite = $edao->update($data);
		if ($reussite){
			$message = "l'équipe [". $_REQUEST['nom'] . "] a bien été modifiée.";
			$data = $edao->findAll();
			return new Page("afficherequipes", "PlayPro - Les équipes", $data, $message);

		}
	}

}

class IntegrerequipeControleur implements Action {
	public function execute(){
		
		if (!ISSET($_REQUEST["nom"]))


			return new Page("accueil", "PlayPro - Accueil", null, null);
		
		if (!ISSET($_SESSION)) session_start();
		
		
		
		$udao = new MembreDAO();
		$edao = new EquipeDAO();

		$user = $udao->findBytitre($_SESSION["connected"]);


		$data = $edao->findBytitre($_REQUEST['nom']);

		if ($data->getNb_max_joueurs()> $data->getNb_joueurs()){
			$user->setEquipe($data->getNom_equipe());

			$edao->integrer($user->getPseudo(), $data->getNom_equipe());

			$message = "Vous êtes maintenant membre de l'équipe [". $_REQUEST['nom'] . "].";
			$data = $edao->findAll();
			return new Page("afficherequipes", "PlayPro - Les équipes", $data, $message);


		}else{
			$message = "l'équipe [". $_REQUEST['nom'] . "] est déjà pleinne.";
			$data = $edao->findAll();
			return new Page("afficherequipes", "PlayPro - Les équipes", $data, $message);
		}



		echo $data->getNom_equipe();

		$reussite = $edao->update($data);
		if ($reussite){
			$message = "l'équipe [". $_REQUEST['nom'] . "] a bien été modifiée.";
			$data = $edao->findAll();
			return new Page("afficherequipes", "PlayPro - Les équipes", $data, $message);

		}
	}

}

class QuitterequipeControleur implements Action {
	public function execute(){
		
		if (!ISSET($_REQUEST["nom"]))


			return new Page("accueil", "PlayPro - Accueil", null, null);
		
		if (!ISSET($_SESSION)) session_start();
		
		
		
		$udao = new MembreDAO();
		$edao = new EquipeDAO();

		$user = $udao->findBytitre($_SESSION["connected"]);


		$data = $edao->findBytitre($_REQUEST['nom']);

		
		$user->setEquipe($data->getNom_equipe());

		$edao->quitter($user->getPseudo(), $data->getNom_equipe());

		$message = "Vousn'êtes plus membre de l'équipe [". $_REQUEST['nom'] . "].";
		$data = $edao->findAll();
		return new Page("afficherequipes", "PlayPro - Les équipes", $data, $message);




		echo $data->getNom_equipe();

		$reussite = $edao->update($data);
		if ($reussite){
			$message = "l'équipe [". $_REQUEST['nom'] . "] a bien été modifiée.";
			$data = $edao->findAll();
			return new Page("afficherequipes", "PlayPro - Les équipes", $data, $message);

		}
	}
}



// Le traitement de parties commence ici

class ModifierpartieControleur implements Action {
	public function execute(){

		if (!ISSET($_SESSION)) session_start();

		if (!ISSET($_REQUEST['id_partie'])){
			
			return new Page("accueil", "PlayPro - Accueil", null, null);
		}

		else{
			

		$pdao = new PartieDAO();
		
		$data = $pdao->findByID($_REQUEST['id_partie']);

		return new Page("modifierpartie", "PlayPro - Modification", $data, null);
	
		}
				
	}
}

class EnregistrermodifpartieControleur implements Action {
	public function execute(){
		
		if (!ISSET($_REQUEST['id_partie'])){
			
			return new Page("accueil", "PlayPro - Accueil", null, null);
		}

		if (!ISSET($_SESSION)) session_start();
		
		echo 'là-bas';


		// $udao = new MembreDAO();
		$pdao = new PartieDAO();

		// $user = $udao->findBytitre($_SESSION["connected"]);
		
		$data = $pdao->findByID($_REQUEST['id_partie']);

		
		$data->setLieu_partie($_REQUEST['lieuPartie']);
		$data->setEquipe_2($_REQUEST['equipe2']);
		$data->setDate_partie_heure($_REQUEST['datePartie']);
		// $data->setHeurePartie($_REQUEST['heurePartie']);
		// $data->setMessageGerant($_REQUEST['messageGerant']);

		// echo $data->getNom_equipe();

		$reussite = $pdao->update($data);
		if ($reussite){
			$message = "la partie [". $_REQUEST['id_partie'] . "] a bien été modifiée.";
			$data = $pdao->findAll();
			return new Page("afficherparties", "PlayPro - Les parties", $data, $message);

		}
		
		
		
		
	}

}