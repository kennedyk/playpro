<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');
require_once('./modele/MembreDAO.class.php');
require_once('./modele/EquipeDAO.class.php');
require_once('./classes/Message.class.php');

class PortailControleur implements Action {
	public function execute(){




		if (!ISSET($_SESSION)) session_start();

		if (isset($_SESSION['connected'])){
			$message = "Vous êtes maintenant sur votre portail";
			return new Page("portail", "PlayPro - Connecté", null, $message);
		}
		

		if (!$this->valide())
		{

			return new Page("accueil", "PlayPro - Accueil", null, null);
		}

		$udao = new MembreDAO();

		$user = $udao->findBytitre($_REQUEST["username"]);
		
		if ($user == null)
			{
				
				$_REQUEST["field_messages"]["username"] = "Utilisateur inexistant.";	
				return new Page("accueil", "PlayPro - Identifiez-vous", null, null);
			}

		
		else if (!password_verify($_REQUEST['password'],$user->getMdp()))
			{
				$_REQUEST["field_messages"]["password"] = "Mot de passe incorrect.";	
				return new Page("accueil", "PlayPro - Mot de passe incorecte", null, null);
			}

		
		$_SESSION["connected"] = $_REQUEST["username"];
		
		
		$message = "Vous êtes maintenant connecté sur votre portail";
		return new Page("portail", "PlayPro - Connecté", null, $message);
	}

	public function valide()
	{
		$result = true;
		if ($_REQUEST['username'] == "")
		{
			$_REQUEST["field_messages"]["username"] = "Donnez votre nom d'utilisateur";
			$result = false;
		}	
		if ($_REQUEST['password'] == "")
		{
			$_REQUEST["field_messages"]["password"] = "Mot de passe obligatoire";
			$result = false;
		}	
		return $result;
	}
}

	