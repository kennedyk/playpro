<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');

class AproposControleur implements Action {
	public function execute(){
		return new Page("apropos", "PlayPro - À propos", null, null);
	}
}
?>