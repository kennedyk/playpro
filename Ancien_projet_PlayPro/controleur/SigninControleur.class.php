<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');
require_once('./modele/MembreDAO.class.php');
require_once('./classes/Message.class.php');

class SigninControleur implements Action {
	public function execute(){
		if (!ISSET($_REQUEST["pseudonyme"]))
			return new Page("login", "PlayPro - Login", null, null);
		if (!$this->valide())
		{
			$message = "Oups, Ce pseudo est déjà utilisé, trouver un autre.";
			return new Page("portail", "PlayPro - Login", null, $message);
		}

		$infos = array();
		

		$infos['pseudo'] = $_REQUEST['pseudonyme'];
		$infos['nom'] = $_REQUEST['nom'];
		$infos['prenom'] = $_REQUEST['prenom'];
		$infos['sexe'] = $_REQUEST['sexe'];
		$infos['sport'] = $_REQUEST['sport'];
		$infos['mdp'] = password_hash($_REQUEST['mdp'], PASSWORD_DEFAULT);
		$infos['courriel'] = $_REQUEST['courriel'];
		
		$date_element = date("Y-m-d");
		$date_str = date('Y') . "-" . date('m') . "-" . date('d');
		

		$infos['date_inscription'] = $date_str;
		
		$infos['type_membre'] = 'joueur';

		if (ISSET($_REQUEST['type_membre'])) {
			$infos['type_membre'] = $_REQUEST['type_membre'];

			$user = new Membre($infos);
			$udao = new MembreDAO();
			$user = $udao->create($user);

			$message = "Le compte du membre " . $infos['pseudo'] . " est créé.";
			return new Page("portail", "PlayPro - Connecté", null, $message);
		}




		
		$user = new Membre($infos);
		$udao = new MembreDAO();
		$user = $udao->create($user);


		if ($user == null)
			{
				
				$_REQUEST["field_messages"]["username"] = "Utilisateur inexistant.";	
				return new Page("login", "PlayPro - Login", null, null);
			}
		
		if (!ISSET($_SESSION)) session_start();
		
		
		$_SESSION["connected"] = $user->getPrenom();
		$message = "Bienvenu " . $_SESSION["connected"] . " dans votre nouvelle plateforme.";
		return new Page("portail", "PlayPro - Connecté", null, $message);
	}
	public function valide()
	{
		$result = false;
		$udao = new MembreDAO();
		$membre = $udao->findBytitre($_REQUEST['pseudonyme']);

		

		if (is_null($membre));
		{
			$result = true;
		}
		return $result;
	}
}
