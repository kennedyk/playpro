<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');

class CommentControleur implements Action {
	public function execute(){
		return new Page("comment", "PlayPro - Comment?", null, null);
	}
}
?>