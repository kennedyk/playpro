<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Data.class.php');
require_once('./modele/classes/Equipe.class.php');
// require_once('./modele/classes/Partie.class.php');
require_once('./modele/classes/Liste.class.php');
require_once('./modele/EquipeDAO.class.php');
require_once('./modele/PartieDAO.class.php');


class AffichermembresControleur implements Action {
	public function execute(){
		
		if (!ISSET($_SESSION)) session_start();
		if (!ISSET($_SESSION["connected"]))	//utilisateur non connecté.
			return new Page("accueil", "Playpro - Accueil", null, null);

		$udao = new MembreDAO();
		$data = $udao->findAll();


		return new Page("afficherjoueurs", "Playpro - Liste des membres", $data, null);
	}

}
class AfficherequipesControleur implements Action {
	public function execute(){
		if (!ISSET($_SESSION)) session_start();
		if (!ISSET($_SESSION["connected"]))	//utilisateur non connecté.
			return new Page("accueil", "Playpro - Accueil", null, null);
		
		$edao = new EquipeDAO();
		

		$data = $edao->findAll();
		$message = "Ici, vous voyez la liste complète  des équipes.";
		//var_dump($data);
		

		return new Data($data, 200);

		//return new Page("afficherequipes", "Playpro - EQUIPES", $data, $message);
	}
}

class AfficherpartiesControleur implements Action {
	public function execute(){

		// echo "Je suis dans AfficherpartiesControleur";
		if (!ISSET($_SESSION)) session_start();

		if (!ISSET($_SESSION["connected"]))	//utilisateur non connecté.
			return new Page("accueil", "Playpro - Accueil", null, null);

		// echo "Je vais dans le DAO";
		$pdao = new PartieDAO();

		if(isset($_REQUEST['id'])){
			$data = $pdao->findByEquipe($_REQUEST['id']);
			$message = "Voici la liste des parties de " . $_REQUEST['id'] . ".";
			
			return new Page("afficherparties", "Playpro - EQUIPES", $data, $message);
		}

		$data = $pdao->findAll();

		$message = "Liste des parties.";
		
		

		return new Page("afficherparties", "Playpro - PARTIES", $data, $message);
	}
}

class AfficherjoueursControleur implements Action {
	public function execute(){

		// echo "Je suis dans AfficherpartiesControleur";
		if (!ISSET($_SESSION)) session_start();

		if (!ISSET($_SESSION["connected"]))	//utilisateur non connecté.
			return new Page("accueil", "Playpro - Accueil", null, null);

		
		$edao = new equipeDAO();
		$data = $edao->findJoueurs($_REQUEST['id']);
		$message = "Voici la liste des joueurs de l'équipe " . $_REQUEST['id'] . ".";

		return new Page("afficherjoueurs", "Playpro - PARTIES", $data, $message);
	}
}
?>