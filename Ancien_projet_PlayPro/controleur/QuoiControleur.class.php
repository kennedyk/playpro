<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');

class QuoiControleur implements Action {
	public function execute(){
		return new Page("quoi", "PlayPro - À propos", null, null);
	}
}
?>