const ENVOYER = "http://localhost/mvc_simplifie_ajax/?action=ajouter";
//const RECEVOIR = "http://localhost/mvc_simplifie_ajax/?action=afficher";

liens = document.getElementsByClassName("menu");

for (var i = 0; i < liens.length; i++) {

    
	// intercepter le clique du lien « afficher les equipes » du menu
	if(liens[i].href.search("afficherequipes") >= 0) {
		liens[i].addEventListener("click", function(e) {
			e.preventDefault();
            // debugger;
            // console.log(e);
            // console.log(e.target.href);
            //ajaxGet(RECEVOIR, function(reponse) {
			ajaxGet(e.currentTarget.href, function(reponse) {

				document.getElementById('content').firstElementChild.textContent = "Liste des equipes";
                document.getElementById('content').firstElementChild.className = "card-panel btn blue center";
				afficherEquipes(JSON.parse(reponse));
				afficherFormulaireGestion();

			})
		});
	}
}

function afficherEquipes(equipes) {

	// effacer le tableau s'il existe déjà
	var tableau = document.getElementById('portail');
    var tableau1 = document.getElementById('equipes');

    debugger;
    //tableau.removeChild(tableau.firstChild);
	if(tableau != null && tableau1 !=null) {
		document.getElementById('content').removeChild(tableau);
        document.getElementById('content').removeChild(tableau1);
	} 

    

	var lignes = new Array();

	// ligne d'entête
	var cellules = new Array();

	for (var attribut in equipes[0]) {
		cellules.push(FabriqueNoeud("th", {textContent: attribut}));
	}

	lignes.push(FabriqueNoeud("tr", {}, cellules));

	// lignes de equipes
	for (var i = 0; i < equipes.length; i++) {

		lignes.push(fabriquerLigneTab(equipes[i]));
	}

	document.getElementById('content').appendChild(FabriqueNoeud("table", {id: "equipes"}, lignes));

}

function fabriquerLigneTab(equipe) {

	var cellules = new Array();
	for (var attribut in equipe) {

		var texte = equipe[attribut];
		if(attribut === "prixUnit") {
			texte = Number(texte).toFixed(2) + ' $';
		}	
		cellules.push(FabriqueNoeud("td", {textContent: texte}));
	}

	return FabriqueNoeud("tr", {}, cellules);
}

function afficherFormulaireGestion() {
	// effacer le formulaire s'il existe déjà
	var formulaire = document.getElementById('formulaire');
	if(formulaire != null) {
		document.getElementById('content').removeChild(formulaire);
	} 

	// Creation d'un bouton le formulaire
	const boutonAjouter = FabriqueNoeud("button", {id: "ajouter", textContent: "Ajouter un equipe"});

	// Creation d'une section pour contenir le formulaire
	// création d'un formulaire et insertion du formulaire dans la section
	const formElt = FabriqueNoeud("form", {id: "formequipe"}, [boutonAjouter]);
	const boiteFormElt = FabriqueNoeud("section", {id: "formulaire"}, [formElt]);

	// insertion de la section "formulaire" après le tableau
	document.getElementById('content').appendChild(boiteFormElt);

	// Ajout d'un listener d'evenement click sur le bouton
	boutonAjouter.addEventListener("click", function(e) {

	    // empeche l'envoie du formulaire
	    e.preventDefault();

	    if(boutonAjouter.textContent === 'Ajouter') {
	        var valide = validerFormulaire();

	        if (valide) {
	            envoyerFormServeur(LireForm());
	            retirerFormulaire(formElt);
	        }
	    } else {
	        afficherFormulaire(formElt);
	    }

	});
}

function afficherFormulaire(formElt) {

    var listeChamps = [{id: "num", name: "num", placeholder: "Code", size: 5, maxlength: 4, pattern: "[A-Z]{1}[0-9]{3}", required: ""},
                       {id: "nom", name: "nom", placeholder: "Nom", size: 45, required: ""},
                       {id: "prix", name: "prix", placeholder: "Prix", type: "input", pattern: "[0-9]*.[0-9]{2}", size: 10, min: 0, required: ""}];

    var messages = ["Format attendu : L000", "Veuillez compléter ce champ", "Format attendu : 00.00"];

    var tooltip = FabriqueNoeud("div", {className: "forminput tooltip"},
                                [FabriqueNoeud("span", {className: "tooltiptext", textContent: "Veuillez compléter ce champ"}),
                                 FabriqueNoeud("input", {})
                                ]);

    // insertion des champs dans le formulaire
    var i = 0;
    listeChamps.forEach(function(champs) {
        // clone et insère le tooltip dans le formulaire
        formElt.insertBefore(tooltip.cloneNode(true), formElt.childNodes[i]);
        
        // message personalisé
        formElt.childNodes[i].childNodes[0].textContent = messages[i];

        // ajoute les attributs au champ de saisie
        for (var attribut in champs) {
            formElt.childNodes[i].childNodes[1].setAttribute(attribut, champs[attribut]);
        }

        i++;
    });

    // modifie le texte du bouton
    formElt.lastChild.textContent = "Ajouter";
}

function validerFormulaire() {

    var valide = true;

    var listeChampsSaisis = document.getElementsByTagName("input");
    
    // la boucle forEach ne peut pas être utilisée sur un tableau d'objets html (HTMLCollection)
    for (var i = 0; i < listeChampsSaisis.length; i++) {

         if (!listeChampsSaisis[i].validity.valid) {
             listeChampsSaisis[i].className = "erreur";
             listeChampsSaisis[i].parentNode.className = "forminput tooltip tooltipvisible";

             listeChampsSaisis[i].addEventListener("input", function(e) {
                // ici this fait référence à l'objet listeChampsSaisis[i] sur lequel on a appliqué la méthode
                // addEventListerner
                 if (!this.validity.valid) {
                     this.className = "erreur";
                     this.parentNode.className = "forminput tooltip tooltipvisible";
                 } else {
                     this.className = "";
                     this.parentNode.className = "forminput tooltip";
                }   
             });

             valide = false;
         }

    }

    return valide;  
}

function retirerFormulaire(formElt) {
    formElt.lastChild.textContent = "Ajouter un equipe";

    var inputsElt = document.getElementsByClassName("forminput");

    while (inputsElt.length!= 0) {
        formElt.removeChild(inputsElt[0]);
    }
}

function LireForm() {

    // récupère les données du formulaire
    var equipe = { 
        num: document.getElementById("num").value,
        denomination: document.getElementById("nom").value,
        prixUnit: document.getElementById("prix").value
    };

    return equipe;

}

function envoyerFormServeur(equipe) {
    ajaxPost(ENVOYER, equipe,
        function (reponse, status) {
            if (status === 'success') {
                afficherequipe(equipe);
                afficherMessage("Le equipe " + equipe['denomination'] + " a été ajouté.");
            } else {
                afficherMessage("Le equipe " + equipe['denomination'] + " n'a pas pu être ajouté.");
            }
        },
        true // Valeur du paramètre isJson
    );
}

function afficherequipe(equipe) {
    var tableau = document.getElementById("equipes");
    tableau.appendChild(fabriquerLigneTab(equipe));
}

function afficherMessage(message) {
    // creation du message
    var messageElt = FabriqueNoeud("div", {id: "message", textContent: message});

    // ajoute le message avant le tableau
    document.getElementById('content').insertBefore(messageElt, document.getElementById('equipes'));

    // fait disparaître le message après 2 secondes
    setTimeout(function () {
            document.getElementById('content').removeChild(document.getElementById("message"));
    }, 2000);
}