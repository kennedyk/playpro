-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  jeu. 18 avr. 2019 à 19:07
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


DROP TABLE partie;
DROP TABLE equipe;
DROP TABLE membre;

--
-- Base de données :  `playpro`
--

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

CREATE TABLE `equipe` (
  `nom_equipe` varchar(100) NOT NULL,
  `capitaine` int(11) DEFAULT NULL,
  `sport` varchar(255) NULL,
  `nb_parties_jouees` int(11) NOT NULL,
  `nb_joueurs` int(11) NOT NULL,
  `nb_max_joueurs` int(11) NOT NULL,
  `date_creation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `equipe`
--

INSERT INTO `equipe` (`nom_equipe`, `capitaine`, `sport`, `nb_parties_jouees`,`nb_joueurs`, `nb_max_joueurs`, `date_creation`) VALUES
('AS Vital', 7, 'Hockey', 0, 3, 8, '2019-05-01'),
('Aviron aviron', 7, 'Badminton', 0, 1, 7, '2019-05-01'),
('FC Champions', 7, 'Soccer', 0, 1, 11, '2019-05-01'),
('Fc Concorde', 14, 'Soccer', 0, 1, 9, '2019-05-01'),
('impact', 2, 'Soccer', 3, 4, 11, '2019-04-03'),
('Invité', 3, NULL, 0, 2, 11, '2019-04-16'),
('Real Madrid', 3, 'Soccer', 1, 0, 11, '2019-04-01'),
('Saint-Vainqueurs', 7, 'Badminton', 0, 1, 6, '2019-05-01'),
('stars', 3, 'Hochey', 0, 1, 6, '2019-04-16');


-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

CREATE TABLE `membre` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `sexe` varchar(20) DEFAULT NULL,
  `sport` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `courriel` varchar(255) NOT NULL,
  `date_inscription` date NOT NULL,
  `type_membre` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `equipe` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `membre`
--

INSERT INTO `membre` (`id`, `pseudo`, `sexe`, `sport`, `nom`, `prenom`, `courriel`, `date_inscription`, `type_membre`, `mdp`, `equipe`) VALUES
(1, 'yami', NULL, '', 'yoyo', 'yoyo', 'y.d@ggg.com', '2019-04-12', '', '$2y$10$Gf7g8Xj43KpSkRWrV4dzoevxB/B4Aajv/5ouL4ofcL32hnuP59WcK', 'AS Vital'),
(2, 'ken', 'Femme', 'Soccer', 'Kalomba', 'Kennedy', 'ra@g.c', '2019-04-13', 'admin', '$2y$10$x5I9CN06da8HezX1DRDf..JNVK9ZMn4TR5DvnDQJKseBiBxcIZEPy', NULL),
(3, 'root', 'Homme', 'Hockey', 'Meilleur', 'Julien', 'root@root.ca', '2019-04-20', 'admin', '$2y$10$Gf7g8Xj43KpSkRWrV4dzoevxB/B4Aajv/5ouL4ofcL32hnuP59WcK', 'Aviron aviron'),
(4, 'lala', 'Homme', 'Hockey', 'lala', 'lala', 'y@k.co', '2019-04-30', 'admin', '$2y$10$D2N2Zrr85tYpAHY5kCt6ruT/HnJ76ZYeUwCb7EFhLUhCbeOmR91uO', NULL),
(5, 'bebe', 'Femme', 'Hockey', 'bebe', 'bebe', 'bebe@b.ca', '2019-05-01', 'joueur', '$2y$10$dURiXdGWpSm90jHv8wCjAulrIe1c8x/iEFGt/V3cnRZEppOUzN2hO', NULL),
(6, 'baba', 'Homme', 'Badmington', 'baba', 'baba', 'b@b.ca', '2019-05-01', 'joueur', '$2y$10$7/HZXXe4HiAH/LTDjPZSJOZrrgPlUbXoBlnQpCjF/dtJvh7IPGoiq', NULL),
(7, 'haha', 'Homme', 'Badmington', 'haha', 'haha', 'al@al.ca', '2019-05-01', 'admin', '$2y$10$Ebz0.VSh9YY9Tvdi.WC2rOI23sizUWPdvjTNY4eOq0o3MXnfmpwya', NULL),
(8, 'jojo', 'Femme', 'Badmington', 'Lemieux', 'Joel', 'al@al.ca', '2019-05-01', 'joueur', '$2y$10$nIta0boEFBhpPMnWxP.hLOD4X8a0njJqPc3O9CcW/wV71GQMA4bqS', NULL),
(9, 'bob', 'Homme', 'Soccer', 'Robert', 'Bob', 'al@al.ca', '2019-05-01', 'joueur', '$2y$10$aFE9jJiEf/IYXqozRf.SSeAtrPt8fNrsCTbK5JUBEfQ.x5hnXGYhy', NULL),
(10, 'rob', 'Homme', 'Hockey', 'Robert', 'Robert', 'al@al.ca', '2019-05-01', 'joueur', '$2y$10$Tyw51VWC4sHqF4p8/CHF6.y64ie8Q9AkyXROELL6toEU11JAzDja.', NULL),
(11, 'val', 'Femme', 'Soccer', 'Valois', 'Valerie', 'al@al.ca', '2019-05-01', 'joueur', '$2y$10$gRfCY1m4p4Vmm2ZFU1n4bOjYCwfRuihPsHjyqhtrkJWxHmYM3aRnC', 'Fc Concorde'),
(12, 'fany', 'Femme', 'Hockey', 'Dadou', 'Fany', 'al@al.ca', '2019-05-01', 'joueur', '$2y$10$pwVlMM/bSILBWKZXzKUVJu2jVf0HqTS6rpg2jXjUDLcVXKVsykHzW', NULL),
(13, 'ely', 'Femme', 'Hockey', 'lili', 'ely', 'al@al.ca', '2019-05-01', 'joueur', '$2y$10$MeqYzEpwStZMWZ7e5Ma4XukBBETkJGtWo8VjrDyvsf12GlaNMYxFi', NULL),
(14, 'lowis', 'Femme', 'Badmington', 'lewis', 'lowis', 'al@al.ca', '2019-05-01', 'joueur', '$2y$10$8cKfMzTK0ueDLOq7eUIl9uZD2CK3ngI5H/USx868ZYFV4DNp5E4ye', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE `partie` (
  `id_partie` int(11) NOT NULL,
  `date_partie_heure` date NOT NULL,
  `sport` varchar(30) NOT NULL,
  `equipe_1` varchar(100) DEFAULT NULL,
  `equipe_2` varchar(100) DEFAULT NULL,
  `lieu_partie` varchar(300) NOT NULL,
  `score` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `partie`
--

INSERT INTO `partie` (`id_partie`, `date_partie_heure`, `sport`, `equipe_1`, `equipe_2`, `lieu_partie`, `score`) VALUES
(1, '2019-04-01', 'Soccer', 'impact', 'Real Madrid', 'Montréal', '0 - 0'),
(2, '2019-04-01', 'Soccer', 'Real Madrid', 'impact', 'Montréal', '0 - 0'),
(3, '2019-04-01', 'Soccer', 'stars', 'impact', 'Anjou', '0 - 0'),
(4, '2019-04-01', 'Badminton', 'impact', 'Real Madrid', 'Montréal', '0 - 0'),
(5, '2019-05-28', 'Soccer', 'impact', 'stars', 'saint loin ', '0 - 0'),
(6, '2019-05-28', 'Soccer', 'impact', 'stars', 'saint loin ', '0 - 0'),
(7, '2019-05-28', 'Soccer', 'impact', 'stars', 'saint loin ', '0 - 0'),
(8, '2019-05-28', 'Hockey', 'impact', 'stars', 'saint loin ', '0 - 0'),
(9, '2019-05-28', 'Soccer', 'impact', 'stars', 'saint loin ', '0 - 0'),
(10, '2019-05-28', 'Soccer', 'impact', 'stars', 'saint loin ', '0 - 0'),
(11, '2019-05-29', 'Soccer', 'Real Madrid', 'Saint-Vainqueurs', 'montreal', '0 - 0'),
(12, '2019-07-19', 'Hockey', 'FC Champions', 'Invité', 'Roberval', '0 - 0'),
(13, '2019-06-06', 'Soccer', 'Fc Concorde', 'Invité', 'Laval', '0 - 0'),
(14, '2019-07-26', 'Hockey', 'Real Madrid', 'impact', 'Rosemont', '0 - 0'),
(15, '2019-07-18', 'Badminton', 'Saint-Vainqueurs', 'Fc Concorde', 'Paris', '0 - 0'),
(16, '2019-08-30', 'Badminton', 'AS Vital', 'Invité', 'Louvres', '0 - 0'),
(17, '2019-07-10', 'Soccer', 'Real Madrid', 'Invité', 'Rosemont', '0 - 0'),
(18, '2019-06-28', 'Badminton', 'AS Vital', 'Invité', 'Laval', '0 - 0'),
(19, '2019-06-14', 'Hockey', 'Invité', 'Invité', 'Montréal', '0 - 0');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`nom_equipe`),
  ADD KEY `Equipe_FK_capitaine` (`capitaine`);

--
-- Index pour la table `membre`
--
ALTER TABLE `membre`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pseudo` (`pseudo`),
  ADD KEY `Membre_FK_nom_equipe` (`equipe`);

--
-- Index pour la table `partie`
--
ALTER TABLE `partie`
  ADD PRIMARY KEY (`id_partie`),
  ADD KEY `Partie_FK_nom_equipe1` (`equipe_1`),
  ADD KEY `Partie_FK_nom_equipe2` (`equipe_2`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `membre`
--
ALTER TABLE `membre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `partie`
--
ALTER TABLE `partie`
  MODIFY `id_partie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `equipe`
--
ALTER TABLE `equipe`
  ADD CONSTRAINT `Equipe_FK_capitaine` FOREIGN KEY (`capitaine`) REFERENCES `membre` (`id`);

--
-- Contraintes pour la table `membre`
--
ALTER TABLE `membre`
  ADD CONSTRAINT `Membre_FK_nom_equipe` FOREIGN KEY (`equipe`) REFERENCES `equipe` (`nom_equipe`);

--
-- Contraintes pour la table `partie`
--
ALTER TABLE `partie`
  ADD CONSTRAINT `Partie_FK_nom_equipe1` FOREIGN KEY (`equipe_1`) REFERENCES `equipe` (`nom_equipe`),
  ADD CONSTRAINT `Partie_FK_nom_equipe2` FOREIGN KEY (`equipe_2`) REFERENCES `equipe` (`nom_equipe`);
