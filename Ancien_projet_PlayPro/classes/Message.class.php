<?php

	class Message {
		protected $succes;
		protected $message;

		public function __construct($succes = false) {
			$this->setSucces($succes);
		}

		public function setSucces($value) {
			$this->succes = $value;
		}

		public function setMessage($value) {
			$this->message = $value;
		}

		public function getSucces() {
			return $this->succes;
		}

		public function getMessage() {
			return $this->message;
		}

	}