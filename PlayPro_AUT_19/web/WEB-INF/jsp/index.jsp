<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="resources/bootstrap/css/bootstrap.min.css"  rel="stylesheet"/>
        <script src="resources/bootstrap/js/bootstrap.min.js" ></script>
        <link href="resources/css/styles.css" rel="stylesheet">
        <script src="resources/js/gestion.js" ></script>
        
    <title>Welcome to Spring Web MVC project</title>
</head>

<body>
    <h1 class="bg-info">Hello! This is the default welcome page for a Spring Web MVC project.</h1>
    <p><i>To display a different welcome page for this project, modify</i>
        <tt>index.jsp</tt> <i>, or create your own welcome page then change
            the redirection in</i> <tt>redirect.jsp</tt> <i>to point to the new
            welcome page and also update the welcome-file setting in</i>
        <tt>web.xml</tt>.</p>
</body>
</html>
